
;vasmm68k_mot -kick1hunks -Fhunkexe -o ../../disk/anglescroll -nosym -no-opt anglescroll.s

 		code_c $40000

plane=$60000
plane2=plane+(50*260)
plane3=plane2+(50*260)
plane4=plane3+(50*260)
plane5=plane4+(50*260)
plane6=plane5+(50*300)	

;>extern "df0:logocon",logo,-1
;>extern "df0:anim3con",anim,-1
;>extern "df0:char0815",char,-1
;>extern	"df0:badcompsound",sound,-1


sysbase=4
openlib=-552
forbid=-132
permit=-138

startlist=38

dmacon=$96
cop1lc=$80
copjmp1=$88

start:
	move.w	#$0,$dff180	
	move.w	#$03e0,$dff096

	clr.w	startflag
	move.l sysbase,a6
	jsr forbid(a6)	



	lea.l $dff000,a5
	move.w #$03e0,dmacon(a5)

	lea	plane,a0
lopit:
	clr.l	(a0)+
	cmp.l	#plane6,a0
	bne.s	lopit		

	bsr makecl
	move.l #copperlist,cop1lc(a5)
	clr.w copjmp1(a5)

	move.w #$0000,$dff180

	move.w #$3081,$dff08e
	move.w #$30c1,$dff090
	move.w #$0038,$dff092
	move.w #$00d0,$dff094


	move.w #$2a71,$dff08e
	move.w #$30e1,$dff090
	move.w #$0018,$dff092
	move.w #$00e0,$dff094


	move.w #%0101001000000000,$dff100
	clr.w $dff102
 	clr.w $dff104
	clr.w $dff108
	clr.w $dff10a

	move.w	#$8240,$dff096
	move.w #$8030,$dff09a
	move.l $6c,ipuf
	bsr blitter
	bsr setcolor
	lea sttext,a0
	jsr string
	move.l #1,d0
	jsr sound

	move.w #$83c0,$dff096

	move.l #irq,$6c


wait:	
	tst.b startflag
	beq wait

	move.l ipuf,$6c

;alte copper-list !
	jsr 	sound+28
	move.w	#15,$dff096
	lea $dff000,a5
	move.l #grname,a1
	clr.l d0
	move.l sysbase,a6
	jsr openlib(a6)
	move.l d0,a4
	move.l startlist(a4),cop1lc(a5)
	clr.w copjmp1(a5)
	move.w #$83e0,dmacon(a5)
	move.l a4,a1
	jsr -414(a6)
	jsr	permit(a6)

ende:	
;	move.b statusword+1,$101
	clr.l d0
	rts


irq:	
	movem.l d0/d1/a0/a1/a5/a6,-(a7)
	lea $dff000,a0
	move.l sysbase,a6
	move.w $1c(a0),d1
	btst #$000e,d1
	beq iende
	and.w $1e(a0),d1
	btst #$0006,d1
	beq skip1
	movem.l $009c(a6),a1/a5
	pea -36(a6)
	jmp (a5)

skip1:	
	btst #$0004,d1
	beq skip2
	movem.l $0084(a6),a1/a5
	pea -36(a6)
	jmp (a5)

skip2:	
	btst #$0005,d1
	beq iende
	movem.l d0-d7/a0-a6,-(a7)
;	move #$f00,$dff180
	move #0,$dff1a0
	move #0,$dff184
	bsr scroll
	bsr dreh
	bsr routine
	jsr maus
	jsr makecl
	jsr sound+14

	move #$0,$dff180

	movem.l (a7)+,d0-d7/a0-a6	
	movem.l $0090(a6),a1/a5
	pea -36(a6)
	jmp (a5)

iende:	
	movem.l (a7)+,d0/d1/a0/a1/a5/a6
	rte


makecl:	
	move.l #copperlist,a0
	move.l #plane,d6
	move.w #$00e0,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00e2,(a0)+
	swap d6
	move.w d6,(a0)+
	move.l #plane2,d6
	move.w #$00e4,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00e6,(a0)+
	swap d6
	move.w d6,(a0)+
	move.l #plane3,d6
	move.w #$00e8,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00ea,(a0)+
	swap d6
	move.w d6,(a0)+
	move.l #plane4,d6
	move.w #$00ec,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00ee,(a0)+
	swap d6
	move.w d6,(a0)+
	move.l #plane5,d6
	move.w #$00f0,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00f2,(a0)+
	swap d6
	move.w d6,(a0)+
	move #$900f,(a0)+
	move #$fffe,(a0)+
	move #$1a0,(a0)+
	move #$fff,(a0)+

	move #$92,d0
	lea colors,a1
	lea wobtab,a2
coplop:	
	move.b d0,(a0)+
	move.b #$d7,(a0)+
	move #$fffe,(a0)+
	move #$184,(a0)+
	move (a1)+,(a0)+
	move #$102,(a0)+
	move (a2)+,(a0)+
	add #1,d0
	cmp #270,d0
	bne coplop




	move.w #$ffff,(a0)+
	move.w #$fffe,(a0)+
	rts


blitter:
	move.w #3,d6
	move.l #logo+64,a0
	move.l #plane+8,a1
blop2:	
	move.w #%0000100111110000,$dff040
	move.w	#0,$dff042
	move.l	a0,$dff050
	move.l	a1,$dff054
	move.w	#10,$dff066
	move.w	#$ffff,$dff044	
	move.w	#$ffff,$dff046
	move.w	#256*64+20,$dff058
blop:	
	btst	#14,$dff002
	bne 	blop
	add.l 	#13000,a1
	add.l 	#10240,a0
	dbra 	d6,blop2
	rts



blitter2:
	move.w #%0000100111110000,$dff040
	move.w	#0,$dff042
	move.l	a0,$dff050
	move.l	a1,$dff054
	move.w	#48,$dff066
	move 	#38,$dff064
	move.w	#$ffff,$dff044	
	move.w	#$ffff,$dff046
	move.w	#16*64+1,$dff058
blop22:
	btst	#14,$dff002
	bne blop22
	rts



maus:	
	move.b	$dff00b,d0
	sub.b old,d0
	bmi links

rechts:	
	ext.w d0
	add.w d0,sx
	jmp endmous

links:	
	ext.w d0
	add.w d0,sx

endmous:
	move.b $dff00b,old

	move.b $dff00a,d0
	sub.b old2,d0
	bmi rauf

runter:	
	ext.w d0
	add.w d0,sy
	cmp #16*5,sy
	blt endmous2
	move	#16*4,sy
	jmp endmous2

rauf:	
	ext.w d0	
	add.w d0,sy
	cmp	#0,sy
	bpl endmous2 
	move	#0,sy
endmous2:
	move.b $dff00a,old2
	clr.l d0
	lea wobtab,a0
	move #120,d6
woplop2:
	move.w #$70,(a0)+
	dbra d6,woplop2
	move sy,d0
	lsr #4,d0
	move d0,d7
	lea wobble,a0
	lea wobtab,a1
	lsl #4,d0
	add.l d0,a1
	add.l #64,a1
	move #7,d6
woblop:	
	move (a0)+,(a1)+
	dbra d6,woblop
	lea wobble,a0
	move wobble,d0
	move #47,d6
scwop:	
	move 2(a0),(a0)	
	add.l #2,a0
	dbra d6,scwop
	move d0,(a0)


auswert: 
	cmp #4,d7
	beq  startgame

	btst #6,$bfe001
	bne rechtsmaus

	bset d7,statusword+1
	lea jatext,a0
	clr.l d6
	move d7,d6
	lsl #3,d6
	add #138,d6	
	move.b d6,jatext+1
	bsr string	
	bra ed
rechtsmaus: 
	btst #10,$dff016
	bne ed
	bclr d7,statusword+1
	lea notext,a0
	clr.l d6
	move d7,d6
	lsl #3,d6
	add #138,d6	
	move.b d6,notext+1
	bsr string	
ed:	
	rts
level:  
	sub #1,waitlevel
	bne ed
	move #5,waitlevel
	btst #6,$bfe001
	bne nomin
	sub.b #1,statusword
	bne skipnull
	move.b #1,statusword
skipnull:
	bra nohund	
nomin:  
	btst #10,$dff016
	bne ed
	add.b #1,statusword
	cmp.b #6,statusword
	bne nohund
	move.b #5,statusword
nohund: 
	clr.l d0
	move.b statusword,d0
	divu #10,d0
	add #48,d0
	move.l #42,d1
	move.l #170,d2
	jsr print
	swap d0
	add #48,d0
	move.l #43,d1
	move.l #170,d2
	jsr print
	rts






jatext: dc.b 41,0," ON",-1,-1
notext: dc.b 41,0,"OFF",-1,-1
statusword: dc.b 01,00
startgame: 
	btst #6,$bfe001
	bne skipdruck
	st startflag
skipdruck:
	rts
waitlevel: dc.w 5
startflag: dc.b 0
	even
wobble: dc.w $10,$10,$10,$20,$20,$30,$40,$50,$60,$70,$80
	dc.w $90,$a0,$b0,$c0,$d0,$d0
	dc.w $e0,$e0,$e0,$f0,$f0,$f0,$f0,$e0,$e0,$e0
	dc.w $d0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40
	dc.w $30,$20,$20,$10,$10,$10,$00,$00,$00,$00,$00

;	rts

sx:	dc.w 0
sy:	dc.w 0
old:	dc.b 0
old2:	dc.b 0
	even


wobtab: 
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	
 dc.w 0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0
 dc.w $e0,$d0,$c0,$b0,$a0,$90,$80,$70,$60,$50,$40,$30,$20,$10,00	



sttext: 
	dc.b 20,106,"SUBWAY",-1
	DC.b 20,114,"IS PROUD TO PRESENT",-1
	DC.b 20,122,"7 GATES OF JAMBALA TRAINER",-1
	DC.b 20,138,"KEYFUNCTIONS        :OFF",-1
	DC.b 20,146,"UNLIMITED LIVES     :OFF",-1
	DC.b 20,154,"UNLIMITED ENERGY    :OFF",-1
	DC.b 20,162,"UNLIMITED GOLD      :OFF",-1
	DC.B 20,170,"      START GAME        ",-1,-1


	even

tab: 
 dc.w 0,2,4,6,8,10,12,14
 dc.w 16,18,20,22,24,26,28,30

 dc.w 0+800,2+800,4+800,6+800,8+800,10+800,12+800,14+800
 dc.w 16+800,18+800,20+800,22+800,24+800,26+800,28+800,30+800

 dc.w 0+1600,2+1600,4+1600,6+1600,8+1600,10+1600,12+1600,14+1600
 dc.w 16+1600,18+1600,20+1600,22+1600,24+1600,26+1600,28+1600,30+1600

 dc.w 0+2400,2+2400,4+2400,6+2400,8+2400,10+2400,12+2400,14+2400
 dc.w 16+2400,18+2400,20+2400,22+2400,24+2400,26+2400,28+2400,30+2400

 dc.w 0+3200,2+3200,4+3200,6+3200,8+3200,10+3200,12+3200,14+3200
 dc.w 16+3200,18+3200,20+3200,22+3200,24+3200,26+3200,28+3200,30+3200


 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1
e1: 
 dc.w 0,15,16,31,32,47,48,63,64,79,65,78,49,62,33,46,17,30,1,14,2,13
 dc.w 18,29,34,45,50,61,66,77,67,76,51,60,35,44,19,28,3,12,4,11,20
 dc.w 27,36,43,52,59,68,75,69,74,53,58,37,42,21,26,5,10,6,9,22,25,38
 dc.w 41,54,57,70,73,71,72,55,56,39,40,23,24,7,8


 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,200

 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
e2:



 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1

 dc.w 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
 dc.w 21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40
 dc.w 41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60
 dc.w 61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,200

 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
e3:
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w 79,78,63,77,62,47,76,61,46,31,75,60,45,30,15,74,59,44,29,14
 dc.w 73,58,43,28,13,72,57,42,27,12,71,56,41,26,11,70,55,40,25,10
 dc.w 69,54,39,24,9,68,53,38,23,8,67,52,37,22,7,66,51,36,21,6
 dc.w 65,50,35,20,5,64,49,34,19,4,48,33,18,3,32,17,2,16,1,0
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,200


 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
e4:
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1

 dc.w 18,19,20,35,51,22,38,54,24,40,56,57,58,27,28,29,44,60
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,31,47,63,79,78,77,76,75
 dc.w 74,73,72,71,70,69,68,67,66,65,64,48,32,16,17,33,49,50,34
 dc.w 36,52,53,37,21,23,39,55,25,41,26,42,43,59,30,46,45,61,62

 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,200

 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
e5:
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w 15,16,47,48,79,14,17,46,49,78,13,18,45,50,77,12,19,44,51,76
 dc.w 11,20,43,52,75,10,21,42,53,74,9,22,41,54,73,8,23,40,55,72
 dc.w 7,24,39,56,71,6,25,38,57,70,5,26,37,58,69,4,27,36,59,68
 dc.w 3,28,35,60,67,2,29,34,61,66,1,30,33,62,65,0,31,32,63,64

 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,200

 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
e6:
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w 0,79,1,78,2,77,3,76,4,75,5,74,6,73,7,72,8,71,9,70,10,69,11
 dc.w 68,12,67,13,66,14,65,15,64,31,48,30,49,29,50,28,51,27,52
 dc.w 26,53,25,54,24,55,23,56,22,57,21,58,20,59,19,60,18,61,17,62
 dc.w 16,63,32,47,33,46,34,45,35,44,36,43,37,42,38,41,39,40

 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,200


 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
e7:
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w 1,17,33,49,65,67,68,69,53,37,21,5,4,3,19,35,36
 dc.w 71,72,73,57,41,25,9,8,7,23,39,40
 dc.w 75,76,77,61,45,29,13,12,11,27
 dc.w 43,59
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
 dc.w 0,16,32,48,64,2,18,34,50,51,52,66,20,6,22,38,54,55,56
 dc.w 70,24,56
 dc.w 10,26,42,58,74,60,44,28,14,15,31,30,46,47,63,62,78,79
 dc.w -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,200


list: dc.l e7,e2,e3,e4,e4,e5,e2,e6,e3,e5,e3,e6,e2,e1,e5,-1
listzeig: dc.l list
zeiger: dc.l e1


colors:
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630
 dc.w $630,$850,$a70,$c90,$c90,$a70,$850,$630


dreh:	
	tst flag
	bne skipall
	sub #1,warte
	bne skipall
	move #2,warte     ;2 is best
drehskip:
	move.l zeiger,a2

	tst auf
	bne wegblend
	move #8,d7
	clr.l d6
	move #0,d6
	bra looop
wegblend:
	move #9,d7
	clr.l d6
	move #9,d6

looop:
	move.l #plane5+662,a1

	move (a2),d0

	cmp #200,d0
	beq end	

	cmp #$ffff,d0
	beq skipit

	lsl #1,d0
	lea tab,a3
	clr.l d2
	move (a3,d0.w),d2
	add.l d2,a1 

	move.l animzeig,a0
;	move.l #anim+[48*40],a0  
	clr.l d0
	move d6,d0
	lsl #1,d0
	add.l d0,a0

    jsr blitter2
skipit:
	sub.l #2,a2
	add #1,d6

	dbra d7,looop
	add.l #2,zeiger
skipall:
	rts

end: 
	move #-1,flag
	rts

animzeig: dc.l anim

routine: 
 	tst flag
	beq rende
lopro:	
	move.l listzeig,a0
	move.l (a0),d0
	cmp.l #-1,d0
	bne skipro
	move.l #list,listzeig
	bra lopro
skipro:	
	move.l d0,zeiger
	add.l #4,listzeig
	move #0,flag
	eor #1,auf
	add.l #640,animzeig

	cmp.l #anim+(64*40),animzeig
	bne rende
	move.l #anim,animzeig
rende: 
	rts


flag: dc.w 0
auf: dc.w 0
warte: dc.w 50	
;wo:dc.l 0

string:	
	clr.l d0
	clr.l d1
	clr.l d2

	move.b (a0)+,d1
	move.b (a0)+,d2
stlop:	
	move.b (a0)+,d0
	cmp.b #-1,d0
	bne nontxt
	move.b (a0),d0
	cmp.b #-1,d0
	beq endst
nontxt2: 
	bra string
nontxt:	
	bsr print
	add #1,d1
	bra stlop
endst: rts
;char in d0,x in d1, y in d2
print:	
	movem.l d0-d2/a0/a1,-(a7)
	mulu #50,d2
	lea plane2,a0
	add.l d2,a0
	add d1,a0
	
	sub #$20,d0
	mulu #128,d0
	lea char,a1
	add.l d0,a1
	
	move #7,d0
charcpy:
	move.b (a1)+,(a0)+
	add.l #3,a1
	add.l #49,a0
	dbra d0,charcpy
	movem.l (a7)+,d0-d2/a0/a1
	rts




setcolor:
	lea $dff180+32,a0
	lea logo,a1
	move #15,d0
setlop:	
	move (a1)+,(a0)+
	dbra d0,setlop
	lea $dff180,a0
	lea logo,a1
	move #15,d0
setlop2:
	move #$0,(a0)+
	dbra d0,setlop2
	rts





setchar:
	lea char,a0
	sub #$20,d0
	mulu #128,d0
	add.l d0,a0
	move #7,d5
qlop2:	
	move #7,d6

qlop:
	move d2,d3	

	lea plane5,a1
	mulu #50,d3
	add.l d3,a1


	move d1,d4
	move d4,d0
	lsr #3,d4
	add.l d4,a1
	and #15,d0
	not d0
	btst d6,(a0) 				
	beq loesch
	bset d0,(a1)
	bra skipitzz
loesch:	
	bclr d0,(a1)
skipitzz:
	add #1,d1
	add #1,d2
	dbra d6,qlop

	add.l #4,a0
	sub #7,d2
	sub #9,d1
	dbra d5,qlop2

	rts

scroll:
	move.l #plane5+5352,a0
	move.l #plane5+5250,a1
	move.w #%1110100111110000,$dff040
	move.w #0,$dff042
	move.l a0,$dff050
	move.l a1,$dff054
	move.w #18,$dff066
	move #18,$dff064
	move.w #$ffff,$dff044	
	move.w #$ffff,$dff046
	move.w #190*64+16,$dff058
blopzz:	
	btst #14,$dff002
	bne blopzz
	sub #2,scrollreg
	bpl skip
	move #7,scrollreg
back:	
	move.l textpt,a0
	clr.l d0
	move.b	(a0),d0
	beq textback
	move #200,d1
	move #280,d2
	bsr setchar
	add.l #1,textpt
skip:
	rts
scrollreg: dc.w 0
textback: 
	move.l #text,textpt
	bra back
textpt: dc.l text
text:

 DC.b "   SHOOBY DOOBY DOO...        ANOTHER SUBWAY TRAINER !  "
 DC.b "      THIS TIME WE BRING YOU THE TRAINER FOR '7 GATES OF "
 DC.b "JAMBALA', AND YOU REALLY NEED A TRAINER FOR THAT GAME !   "
 DC.b "  THANX TO VISION FACTORY FOR THE CRACK !       "
 DC.b "  GREETINX GO TO ALL OUR FRIENDS AND CONTACTS !           "
 DC.b " FOR OTHER COOL SUBWAY PRODUCTIONS AND TRAINERS WRITE TO: "
 DC.b "SUBWAY, P.O.BOX 85, 5017 SALZBURG, AUSTRIA (OF COURSE !)     "
 DC.b "OR LOOK IN THE SWEDISH PLEASURE DOME (2400-9600 BAUD):   DIAL"
 DC.b " +46/16127263  !!!      "
 DC.b "         INTRO CODED BY TILT   ---   GRAPHIX BY BAD ARGS  ---  "
 DC.b "SOUND BY THE MANIACS OF NOISE                                 " 
 DC.b "AND AT LAST:  THIS TRAINER WAS DONE FROM A QUARTEX CRACK "
 DC.b "                                                    ",0




grname:	dc.b "graphics.library",0
	even

ipuf:	dc.l 0





copperlist:
	blk.w 4*1000

iffcolors: blk.w 32	

logo:   incbin "logocon"
;blk.b 41024,0 
anim: incbin "anim3con"
;   blk.b 10240,0

char: incbin "char0815"
; blk.b8192,0

sound: incbin "badcompsound"
;	blk.b $7e00,0

fini:


