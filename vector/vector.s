; vasmm68k_mot -kick1hunks -Fhunkexe -o vector -nosym vector.s

  org $40000 
  code_c

sysbase=4
openlib=-552
forbid=-132
permit=-138

startlist=38

dmacon=$96
cop1lc=$80
copjmp1=$88

plane1=$48000        ;blk.w 25*260,0
plane1b=$48000+13000 ;blk.w 25*260,0
plane2=$48000+26000  ;blk.w 25*260,0
plane3=$48000+39000  ;blk.w 25*260,0

start:	move.l sysbase,a6
	jsr forbid(a6)	
	move #0,stflag
	
	lea.l $dff000,a5
	move #0,$dff180
	move.w #$03e0,dmacon(a5)
	
	lea $48000,a0
clrlop:	clr.l (a0)+
	cmp.l #$52000,a0
	bne clrlop

	lea $7b000,a0
clrlop2:clr.l (a0)+
	cmp.l #$7e800,a0
	bne clrlop2

	bsr makecl
         move.l #copperlist,cop1lc(a5)
	clr.w copjmp1(a5)

	move.w #$0000,$dff180
	move.w #$0eee,$dff182
	move.w #$0f00,$dff184
	move.w #$0f00,$dff186
	
	move.w #$2a71,$dff08e
	move.w #$30e1,$dff090
	move.w #$0018,$dff092
	move.w #$00e0,$dff094
	move.w #%0010001000000000,$dff100
	clr.w $dff102
	move.w #0,$dff104
	clr.w $dff108
	clr.w $dff10a
	cmp #1,sinu
	beq not
	jsr div
not:	move.w #$83c0,dmacon(a5)
	move.w #$8010,$dff09a



	move.l $6c,ipuf

	lea text,a0
	bsr string


	move.l #irq,$6c


wait:	
	cmp #0,nowflag
	bne wait


	bsr clr
	bsr bwait
	bsr dreh
	bsr con3d	
	bsr bwait
	bsr draw
	bsr bwait
	move #1,nowflag

	tst stflag
	beq wait


	move.w #$000f,$dff096
	move.l ipuf,$6c

;alte copper-list !
	lea $dff000,a5
	move.l #grname,a1
	clr.l d0
	move.l sysbase,a6
	jsr openlib(a6)
	move.l d0,a4
	move.l startlist(a4),cop1lc(a5)
	clr.w copjmp1(a5)
	move.w #$83e0,dmacon(a5)
	jsr permit(a6)

ende:	move stflag,d0
	rts



nowflag:dc.w 0

bwait:; move #$00f0,$dff180
	btst #14,$dff002
	bne bwait
;	move#$0,$dff180
	rts

irq:	movem.l d0/d1/a0/a1/a5/a6,-(a7)
	lea $dff000,a0
	move.l sysbase,a6
	move.w $1c(a0),d1
	btst #$000e,d1
	beq iende
	and.w $1e(a0),d1
	btst #$0006,d1
	beq skip1
	movem.l $009c(a6),a1/a5
	pea -36(a6)
	jmp (a5)

skip1:	btst #$0004,d1
	beq skip2
	movem.l d0-d7/a0-a6,-(a7)
;	move#$0f00,$dff180
	cmp #0,nowflag
	beq skipirq
;	move #$0f00,$dff180
	move.l pladr2,d0
	move.l pladr,pladr2
	move.l d0,pladr
	
;	swap d0
;	move d0,copperlist+2
;	swap d0
;	move d0,copperlist+6	
	move #0,nowflag

skipirq:
	move color,$dff182
	bsr scroll
	bsr makecl
	bsr maus
	bsr blend
	sub #1,warted
	bne skipit
	move #5,warted
	bsr where

skipit:
;	move #$000,$dff180

	movem.l (a7)+,d0-d7/a0-a6


	movem.l $0084(a6),a1/a5
	pea -36(a6)
	jmp (a5)

skip2:	btst #$0005,d1
	beq iende



	movem.l $0090(a6),a1/a5
	pea -36(a6)
	jmp (a5)

iende:	movem.l (a7)+,d0/d1/a0/a1/a5/a6
	rte
warted:dc.w 7

makecl:	move.l #copperlist,a0
	move.l pladr,d6
	move.w #$00e0,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00e2,(a0)+
	swap d6
	move.w d6,(a0)+
	move.l #plane2,d6
	move.w #$00e4,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00e6,(a0)+
	swap d6
	move.w d6,(a0)+
	move.l #plane3,d6
	move.w #$00e8,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00ea,(a0)+
	swap d6
	move.w d6,(a0)+

	move #$37+36,d0
	lea colors,a1
	
coplop1:move.b d0,(a0)+
	move.b #$0f,(a0)+
	move #$fffe,(a0)+
	move #$184,(a0)+
	move (a1),(a0)+
	move #$186,(a0)+
	move (a1)+,(a0)+
	add #1,d0
	cmp #237,d0
	bne coplop1
	move #$102,(a0)+
	move scrollreg,(a0)+

	lea colors2,a1
	move #241,d0
coplop2:move.b d0,(a0)+
	move.b #$0f,(a0)+
	move #$fffe,(a0)+
	move #$184,(a0)+
	move (a1)+,(a0)+
	add #1,d0
	cmp #255,d0
	bne coplop2

	move #$ff0f,(a0)+
	move #$fffe,(a0)+
	lea colors2,a1
	

	move #$102,(a0)+
	move #$0,(a0)+

	move #$9c,(a0)+
	move #$8010,(a0)+


	move.w #$ffff,(a0)+
	move.w #$fffe,(a0)+
	rts


r2b:	dc.l 0


rpos:	dc.w 0


line:	lea oktabelle,a3
	move.l pladr2,a0
	move.l #50,a1
	move.l #$ffffffff,a2
	move.l a1,d4
	mulu d1,d4
	moveq #-$10,d5
	and.w d0,d5
	lsr.w #3,d5
	add.w d5,d4
	add.l  a0,d4
	clr.l d5
	sub.w d1,d3
	roxl.b #1,d5
	tst.w d3
	bge y2gy1
	neg.w d3
y2gy1:	sub.w d0,d2
	roxl.b #1,d5
	tst.w d2
	bge x2gx1
	neg.w d2
x2gx1:	move.w d3,d1
	sub.w d2,d1
	bge dygdx
	exg d2,d3
dygdx:	roxl.b #1,d5
 	move.b (a3,d5),d5
	add.w d2,d2
;	move#$f00,$dff180
waitlop:btst #14,$dff002
	bne waitlop
;	move#$0,$dff180
	move.w d2,$dff062
	sub.w d3,d2
	bge signnl
	or.b #$40,d5
signnl:	move.w d2,$dff052
	sub.w d3,d2
	move.w d2,$dff064
	move.w #$8000,$dff074
	move.w a2,$dff072
	move.w #$ffff,$dff044
	and.w #$000f,d0
	ror.w #4,d0
	or.w #$0bca,d0
	move.w d0,$dff040
	move.w d5,$dff042
	move.l d4,$dff048
	move.l d4,$dff054
	move.w a1,$dff060
	move.w a1,$dff066
	lsl.w #6,d3
	addq.w #2,d3
	move.w d3,$dff058
	rts	




con3d:	move.l figur,a6		;Struktur der Figur nach a6
	move (a6),d7		;Anzahl der Figurenpunkte nach d7
	sub #1,d7

	move winkela,d1		;Rotationswinkel um X-Achse
	move winkelb,d2		;     - " -      um Y-Achse
	move winkelc,d3		;     - " -      um Z-Achse

	move d1,d0		
	bsr sinus		;Sinus und Cosinus des ersten	
	move d0,sina		;Winkels berechnen
	move d1,d0
	bsr cosin
	move d0,cosa
	
	move d2,d0
	bsr sinus		;Sinus und Cosinus des 2. Winkels
	move d0,sinb
	move d2,d0
	bsr cosin
	move d0,cosb

	move d3,d0
	bsr sinus		;Sinus und Cosinus des 3. Winkels
	move d0,sinc
	move d3,d0
	bsr cosin
	move d0,cosc

	
	move cosb,d0
	move cosc,d1
	muls d1,d0
	asr.l #7,d0
	move d0,cosbcosc

	move cosb,d0
	move sinc,d1
	muls d1,d0
	asr.l #7,d0
	move d0,cosbsinc
	

	move sina,d0
	move cosb,d1
	muls d1,d0
	asr.l #7,d0
	move d0,sinacosb

	move sina,d0
	move sinb,d1
	muls d1,d0
	asr.l #7,d0
	move d0,sinasinb

	move sinc,d1
	muls d1,d0
	asr.l #7,d0
	move d0,sinasinbsinc

	move cosa,d0
	move cosc,d1
	muls d1,d0
	asr.l #7,d0
	move d0,cosacosc

	move cosa,d0
	move sinc,d1
	muls d1,d0
	asr.l #7,d0
	move d0,cosasinc

	move sinasinb,d0
	move cosc,d1
	muls d1,d0
	asr.l #7,d0
	move d0,sinasinbcosc

	move sina,d0
	move sinc,d1
	muls d1,d0
	asr.l #7,d0
	move d0,sinasinc

	move sina,d0
	move cosc,d1		
	muls d1,d0
	asr.l #7,d0
	move d0,sinacosc

	move cosa,d0
	move cosb,d1
	muls d1,d0
	asr.l #7,d0
	move d0,cosacosb

	move cosacosc,d0
	move sinb,d1
	muls d1,d0
	asr.l #7,d0
	move d0,cosacoscsinb
	
	move cosasinc,d0
	move sinb,d1
	muls d1,d0
	asr.l #7,d0
	move d0,cosasincsinb

	move sinasinbcosc,d0
	sub cosasinc,d0
	move d0,ddd

	move sinasinbsinc,d0
	add cosacosc,d0
	move d0,eee

	move cosacoscsinb,d0
	add sinasinc,d0
	move d0,ggg

	move cosasincsinb,d0
	sub sinacosc,d0
	move d0,hhh

	move.l a6,a0		;Zeiger auf Figuren -> a0
	add.l #8,a0		;Zeiger auf das 1. Punktetripel
	lea puff,a1		;Zeiger auf Puffer
	
conlop:	move (a0)+,d0		;X-Koordinate nach d0
	move (a0)+,d1		;Y-Koordinate nach d1
	move (a0)+,d2		;Z-Koordinate nach d2
	
	move d0,d3		;Berechnungen
	muls cosbcosc,d3
	asr.l #7,d3

	move d1,d4
	muls cosbsinc,d4
	asr.l #7,d4

	move d2,d5
	muls sinb,d5
	asr.l #7,d5

	add d4,d3
	sub d5,d3
	muls scal,d3
	asr.l #7,d3
	move d3,xkor

	
	move d0,d3		;Y-Koordinate berechnen
	muls ddd,d3
	asr.l #7,d3

	move d1,d4
	muls eee,d4
	asr.l #7,d4
	
	move d2,d5
	muls sinacosb,d5
	asr.l #7,d5
	add d5,d4
	add d4,d3
	muls scal,d3
	asr.l #7,d3
	move d3,ykor

				;z-koordinate 
	move d0,d3
	muls ggg,d3
	asr.l #7,d3

	move d1,d4
	muls hhh,d4
	asr.l #7,d4

	move d2,d5
	muls cosacosb,d5
	asr.l #7,d5

	add d3,d4
	add d4,d5
	muls scal,d5
	asr.l #7,d5
	add abstand,d5
		
	add augd,d5		;d5=zkor+augd	
	

	move xkor,d0
	muls augd,d0
	divs d5,d0		
	add #210,d0
	move d0,(a1)+

	move ykor,d0
	muls augd,d0
	divs d5,d0
	add #110,d0
	move d0,(a1)+



	dbra d7,conlop		;Schleife (je nach Punkteanzahl)
	rts


augd:dc.w 200
xkor:dc.w 0
ykor:dc.w 0
zkor:dc.w 0


draw: 	move.l figur,a6
	move 2(a6),d6
	subq #1,d6
	move.l #0,d7
	lea puff,a4
	move.l 4(a6),a5

drawlop:move (a5,d7.w),d4
	lsl #2,d4
	move (a4,d4.w),d0
	move 2(a4,d4.w),d1	
	move 2(a5,d7.w),d4
	lsl #2,d4
	move (a4,d4.w),d2
	move 2(a4,d4.w),d3


	cmp d0,d2
	bne skipi
	cmp d1,d3
	bne skipi
	bra down
skipi:	bsr line
down:	add.w #4,d7
	subq #1,d6
	bpl drawlop
	rts


sinus:	lea sinu,a0
	add.w d0,d0
	move.w (a0,d0.w),d0 
	rts

cosin:  lea sinu+180,a0
	add.w d0,d0
	move.w (a0,d0.w),d0 
        rts

dreh:   add #3,winkelc
	cmp #360,winkelc
	bne drehsk
	move.w# 0,winkelc
drehsk:	add #6,winkelb
	cmp #360,winkelb
	bne ddskip
	move #0,winkelb

ddskip:	add #5,winkela
	CMP #360,winkela
	bne ddskip2
	move #0,winkela
ddskip2:tst ausblend
	bne tto
	subq #1,verz
	cmp #0,verz
	bne ddkipo	
	move #200,verz
tto:	move #1,ausblend
	tst jetzt	
	beq ddkipo	
	clr jetzt
	clr ausblend 
ppgo:	move.l fp,a0
	move.l (a0)+,figur
	tst.l figur
	bne skipp
	move.l #ftab,fp
	bra ppgo

skipp:	move.l a0,fp

scskip:
ddkipo:	rts
blend: sub #1,blverz
	bne skipb	
	move #5,blverz
	tst ausblend
	beq einbl
	sub #$111,color
	bpl skipb
	move #1,jetzt
	move #0,color
skipb:	rts
einbl:  add #$111,color
	cmp #$fff+$111,color
	bne skipb
	move #$fff,color
	rts
blverz: dc.w 5
jetzt: dc.w 0
color: dc.w 0
ausblend: dc.w 0
abstand: dc.w 0
verz: dc.w 200
div:	lea sinu,a0
	move #450,d1
dlop:	move (a0),d0
	asr #8,d0
	tst d0
	bne dskip
	addq #1,d0
dskip:	move d0,(a0)+
	dbra d1,dlop
	rts


clr:	move.l pladr2,a1
blop2:	move.w #%0000000111110000,$dff040
	move.w #0,$dff042
	move.l a0,$dff050
	move.l a1,$dff054
	move.w #0,$dff074
	move.w #0,$dff066
	move.w #$ffff,$dff044	
	move.w #$ffff,$dff046
	move.w #250*64+22,$dff058
	rts

maus:	
mgo:	move.b $dff00b,d0
	sub.b old,d0
	bmi links

rechts:	ext.w d0
	add.w d0,sx
	jmp endmous

links:	ext.w d0
	add.w d0,sx

endmous:move.b $dff00b,old

	move.b $dff00a,d0
	sub.b old2,d0
	bmi rauf

runter:	ext.w d0
	add.w d0,sy
	jmp endmous2

rauf:	ext.w d0	
	add.w d0,sy

endmous2:move.b $dff00a,old2
	cmp.w #0,sy
	bpl endmous3
	move.w #0,sy
endmous3:cmp.w #24,sy
	blt endmous4
	move.w #24,sy
endmous4:
	rts
old: dc.b 0
old2: dc.b 0
sx: dc.w 0
sy: dc.w 0

where:  move sy,d0
	lsr #4,d0
	move d0,d7
	mulu #12,d0
	lsl #1,d0
	lea hier,a1
	move #7,d2
whlop2:	lea redcolors,a2
	move #11,d3
whlop:	move (a2)+,(a1)+	
	dbra d3,whlop
	dbra d2,whlop2
	lea hier,a1
	lea markcolors,a2
	move #11,d6
whlop3:	move (a2)+,(a1,d0.w)
	add #2,a1
	dbra d6,whlop3
	cmp #0,d7
	beq first
	cmp #1,d7
	beq second	
	rts



first:    btst #6,$bfe001
	bne endskip
	move #1,stflag
	rts
second:    btst #6,$bfe001
	bne endskip
	move #2,stflag
	rts
endskip:rts



stflag: dc.w 0
statusword: dc.b 1,4
string:	clr.l d0
	clr.l d1
	clr.l d2
strlop3:move.b (a0)+,d1
	move.b (a0)+,d2
		
strlop2:move.b (a0)+,d0
	beq stend	
	jsr horprint
	clr.l d0
	add #1,d1	
	bra strlop2
stend:	move.b (a0),d0
	beq stend2
	bra strlop3
stend2:	rts



horprint:
	move.l #charset,a5
	lea plane2,a4
	move d2,d3
	mulu #50,d3
	add.l d3,a4
	add.l d1,a4
	sub #$20,d0
	mulu #128,d0
	add.l d0,a5
	move #10,d7
plop:	move.b (a5),(a4)
	add.l #4,a5
	add.l #50,a4
	dbra d7,plop
	rts

scroll: tst scrollwait
	beq scrolli
	sub #1,scrollwait
	rts
scrolli:move scr,d0
	sub #4,d0

scsskip: and #7,d0
	move d0,scr
	cmp #7,d0
	bne skipo

	bsr hscroll
	
skipo:	move scr,d0
	lsl #4,d0
	and #$f0,d0
	move d0,scrollreg
	rts
scr: dc.w 15
hscroll: clr.l d0
	lea plane2+10000,a1
	move #500,d6
sclop:	move.b (a1),-1(a1)
	add.l #1,a1
	dbra d6,sclop
backtext: move.l textpoint,a2
	move.b (a2),d0
backtext2: cmp #2,d0
	beq stop
	cmp #0,d0
	beq textreset
	move #49,d1
	move #200,d2
	bsr horprint
	add.l #1,textpoint
	rts
stop: move #86,scrollwait
	move.b 1(a2),d0
	bra backtext2

scrollwait: dc.w 0
textreset: move.l #sctext,textpoint
	bra backtext
scrollreg: dc.w 15
textpoint: dc.l sctext

sctext: 

  DC.b "     SUBWAY IS PROUD TO PRESENT YOU       ",2
  DC.b "            ODD DISK #1 !!!              ",2
  DC.b " THIS IS THE FIRST DISK OF A NEW SERIES OF FINE COMPACTINGS BY "
  DC.b "MATHEW AND TILT OF SUBWAY. WITH THIS FIRST RELEASE WE BRING YOU "
  DC.b "SILKWORM AND WICKED. ENJOY IT (ESPECIALLY THE SUBWAY-SILKWORM-"
  DC.b "MEGATRAINER.).    LOOK OUT FOR NEW SUBWAY PRODUCTIONS !      "
  DC.b "THANX GO TO THE ACCUMULATORS FOR THE TRAINER OF WICKED.  "
  DC.b "  TODAY'S REGARDS FLY TO:  VISION FACTORY  ---  ORACLE  ---  "
  DC.b "BLACK MONKS  ---  SCOOPEX  ---  ESCAPE  ---  ODYSSEY  ---  "
  DC.b "IPEC  ---  FAIRLIGHT  ---  SUPREME  ---  AMNESIA  ---  AND "
  DC.b "TO SPREADPOINT+DEFJAM+CCS !    PERSONAL GREETS OF MATHEW AND TILT "
  DC.b "GO TO ALL FORMER BREAKFAST CLUB MEMBERS AND CONTACTS !    "
  DC.b "     KLICK BUTTON OR FUCK A HORSE !                         "
  DC.b "                                             ",0 


text: 	dc.b 18,50+12, "      SUBWAY     ",0
	DC.B 18,62+12, " PROUDLY PRESENTS",0	
	dc.b 18,74+12, "  ODD DISK NO. 1",0
	DC.B 10,110+12,"   SILKWORM MEGATRAINER (SUBWAY)  ",0
	DC.B 11,122+12,"     WICKED + (ACCUMULATORS)     ",0
	dc.b 15,122+36,"SELECT WITH RED COLORBAR !",0,0

  even			

odddisk: dc.w 8+18+9+4+12+11
	dc.w 8+18+9+4+12+11
	dc.l odddisklines
	dc.w -120,-15,0
	dc.w -120,15,0
	dc.w -90,15,0
	dc.w -90,-15,0
	dc.w -110,-5,0
	dc.w -110,5,0
	dc.w -100,5,0
	dc.w -100,-5,0

	dc.w -85,-15,0
	dc.w -85,15,0
	dc.w -75,15,0
	dc.w -55,10,0
	dc.w -55,-10,0
	dc.w -75,-15,0
	dc.w -75,-5,0
	dc.w -75,5,0
	dc.w -65,0,0

	dc.w -50,-15,0
	dc.w -50,15,0
	dc.w -40,15,0
	dc.w -20,10,0
	dc.w -20,-10,0
	dc.w -40,-15,0
	dc.w -40,-5,0
	dc.w -40,5,0
	dc.w -30,0,0

	dc.w 0,-15,0
	dc.w 0,15,0
	dc.w 10,15,0
	dc.w 30,10,0
	dc.w 30,-10,0
	dc.w 10,-15,0
	dc.w 10,-5,0
	dc.w 10,5,0
	dc.w 20,0,0

	dc.w 35,-15,0
	dc.w 35,15,0
	dc.w 45,15,0
	dc.w 45,-15,0


	dc.w 50,-15,0
	dc.w 50,3,0
	dc.w 70,3,0
	dc.w 70,8,0
	dc.w 50,8,0
	dc.w 50,15,0
	dc.w 80,15,0
	dc.w 80,-3,0	
	dc.w 60,-3,0
	dc.w 60,-8,0
	dc.w 80,-8,0
	dc.w 80,-15,0

	dc.w 85,-15,0
	dc.w 85,15,0
	dc.w 95,15,0
	dc.w 95,5,0
	dc.w 105,15,0
	dc.w 115,15,0
	dc.w 100,0,0
	dc.w 115,-15,0
	dc.w 105,-15,0
	dc.w 95,-5,0
	dc.w 95,-15,0


odddisklines:
  dc.w  0,1,1,2,2,3,3,0,4,5,5,6,6,7,7,4
  dc.w 8,9,9,10,10,11,11,12,12,13,13,8
  dc.w 14,15,15,16,16,14	
  dc.w 17,18,18,19,19,20,20,21,21,22,22,17
  dc.w 23,24,24,25,25,23
  dc.w 26,27,27,28,28,29,29,30,30,31,31,26
  dc.w 32,33,33,34,34,32
  dc.w 35,36,36,37,37,38,38,35
  dc.w 39,40,40,41,41,42,42,43,43,44,44,45,45,46,46,47
  dc.w 47,48,48,49,49,50,50,39
  dc.w 51,52,52,53,53,54,54,55,55,56,56,57,57,58,58,59,59,60
  dc.w 60,61,61,51

subway: dc.w 39+24
	dc.w 39+24
	dc.l subwaylines

ps:	dc.w -100,-15,30
	dc.w -100,3,30
	dc.w -80,3,30
	dc.w -80,8,30
	dc.w -100,8,30
	dc.w -100,15,30
	dc.w -70,15,30
	dc.w -70,-3,30
	dc.w -90,-3,30
	dc.w -90,-8,30
	dc.w -70,-8,30
	dc.w -70,-15,30

pu:	dc.w -65,-15,30
	dc.w -65,15,30
	dc.w -35,15,30
	dc.w -35,-15,30
	dc.w -45,-15,30
	dc.w -45,5,30
	dc.w -55,5,30
	dc.w -55,-15,30

pb:	dc.w -30,-15,30
	dc.w -30,15,30
	dc.w 0,10,30
	dc.w -10,0,30
	dc.w 0,-10,30

	dc.w -23,-5,30
	dc.w -23,5,30
	dc.w -13,5,30
	dc.w -18,0,30
	dc.w -13,-5,30	

pw: 	dc.w 5,-15,30
	dc.w 5,15,30
	dc.w 35,15,30
	dc.w 35,-15,30
	dc.w 25,-15,30
	dc.w 25,10,30
	dc.w 20,0,30
	dc.w 15,10,30
	dc.w 15,-15,30	

pa: 	dc.w 40,-15,30
	dc.w 40,15,30
	dc.w 50,15,30
	dc.w 50,5,30
	dc.w 60,5,30
	dc.w 60,15,30
	dc.w 70,15,30
	dc.w 70,-15,30
	dc.w 50,-10,30
	dc.w 50,0,30
	dc.w 60,0,30
	dc.w 60,-10,30

py:     dc.w 75,-15,30
	dc.w 75,5,30
	dc.w 85,5,30
	dc.w 85,15,30
	dc.w 95,15,30
	dc.w 95,5,30
	dc.w 105,5,30
	dc.w 105,-15,30
	dc.w 95,-15,30
	dc.w 95,-5,30
	dc.w 85,-5,30
	dc.w 85,-15,30
subwaylines:
  dc.w 0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,0
  dc.w 12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,12
  dc.w 20,21,21,22,22,23,23,24,24,20,25,26,26,27
  dc.w 27,28,28,29,29,25	
  dc.w 30,31,31,32,32,33,33,34,34,35,35,36,36,37,37,38,38,30
  dc.w 39,40,40,41,41,42,42,43,43,44,44,45,45,46,46,39
  dc.w 47,48,48,49,49,50,50,47
  dc.w 51,52,52,53,53,54,54,55,55,56,56,57,57,58,58,59,59,60
  dc.w 60,61,61,62,62,51




puff: 	blk.w 4*100,0

figur:	dc.l subway

ftab:	dc.l odddisk,subway,0
    fp:	dc.l ftab

winkela:	dc.w 0
winkelb:	dc.w 0
winkelc:	dc.w 0


	
sina:	dc.w 0
sinb:	dc.w 0
sinc:	dc.w 0
cosa:	dc.w 0
cosb:	dc.w 0
cosc:	dc.w 0
ddd:    dc.w 0
eee:    dc.w 0
ggg:    dc.w 0
hhh:    dc.w 0
negsinb:	dc.w 0
cosbcosc:	dc.w 0
cosbsinc:	dc.w 0
cosasinc:	dc.w 0
cosacosc:	dc.w 0
sinacosb:	dc.w 0
sinasinb:	dc.w 0
sinasinc:	dc.w 0
sinacosc:	dc.w 0
cosacosb:	dc.w 0
cosasincsinb:	dc.w 0
cosacoscsinb:	dc.w 0
sinasinbsinc:	dc.w 0
sinasinbcosc:	dc.w 0



scal:	dc.w 100
scp:	dc.w 0

oktabelle:
  dc.b 0*4+1,4*4+1,2*4+1,5*4+1,1*4+1,6*4+1,3*4+1,7*4+1


    even
	
grname:	dc.b "graphics.library", 0

	even

ipuf:	dc.l 0,0
rett:	dc.l 0,0



copperlist:
	blk.w 4000
	
pladr:	dc.l plane1
pladr2:	dc.l plane1b

charset:	incbin "charset"
	even
markcolors:

	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	
redcolors:

	dc.w $323,$343,$363,$383,$3a3,$3c3
	dc.w $3e3,$3a3,$383,$363,$343,$323


colors2:dc.w $323,$343,$363,$383,$3a3,$3c3
	dc.w $3e3,$3a3,$383,$363,$343,$323
	




colors:	dc.w $323,$343,$363,$383,$3a3,$3c3
	dc.w $3e3,$3a3,$383,$363,$343,$323
	dc.w $323,$343,$363,$383,$3a3,$3c3
	dc.w $3e3,$3a3,$383,$363,$343,$323
	dc.w $323,$343,$363,$383,$3a3,$3c3
	dc.w $3e3,$3a3,$383,$363,$343,$323
	dc.w $323,$343,$363,$383,$3a3,$3c3
	dc.w $3e3,$3a3,$383,$363,$343,$323



	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
hier:	
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222
	dc.w $222,$422,$622,$822,$a22,$c22
	dc.w $e22,$a22,$822,$622,$422,$222

sinu:
  dc.w $0010,$023c,$0478,$06b3,$08ee,$0b28,$0d61,$0f99,$11d0
  dc.w $1406,$163a,$186c,$1a9d,$1ccb,$1ef7,$2121
  dc.w $2348,$256c,$278e,$29ac,$2bc7,$2ddf,$2ff3
  dc.w $3203,$3410,$3618,$381d,$3a1c,$3c18,$3e0e
  dc.w $4000,$41ed,$43d4,$45b7,$4794,$496b,$4b3d
  dc.w $4d08,$4ece,$508e,$5247,$53fa,$55a6,$574c
  dc.w $58eb,$5a82,$5c13,$5d9d,$5f1f,$609a,$620e
  dc.w $637a,$64de,$663a,$678e,$68da,$6a1e,$6b5a
  dc.w $6c8d,$6db8,$6eda,$6ff4,$7104,$720d,$730c
  dc.w $7402,$74ef,$75d3,$76ae,$7780,$7848,$7907
  dc.w $79bc,$7a68,$7b0b,$7ba3,$7c33,$7cb8,$7d34
  dc.w $7da6,$7e0e,$7e6d,$7ec1,$7f0c,$7f4c,$7f83
  dc.w $7fb0,$7fd3,$7fec,$7ffb
  dc.w $7fff,$7ffb,$7fec,$7fd3,$7fb0,$7f83,$7f4c
  dc.w $7f0c,$7ec1,$7e6d,$7e0e,$7da6,$7d34,$7cb8
  dc.w $7c33,$7ba3,$7b0b,$7a68,$79bc,$7907,$7848
  dc.w $7780,$76ae,$75d3,$74ef,$7402,$730c,$720d
  dc.w $7104,$6ff4,$6eda,$6db8,$6c8d,$6b5a,$6a1e
  dc.w $68da,$678e,$663a,$64de,$637a,$620e,$609a
  dc.w $5f1f,$5d9d,$5c13,$5a82,$58eb,$574c,$55a6
  dc.w $53fa,$5247,$508e,$4ece,$4d08,$4b3d,$496b
  dc.w $4794,$45b7,$43d4,$41ed,$4000,$3e0e,$3c18
  dc.w $3a1c,$381d,$3618,$3410,$3203,$2ff3,$2ddf
  dc.w $2bc4,$29ac,$278e,$256c,$2348,$2121,$1ef7
  dc.w $1ccb,$1a9d,$186c,$163a,$1406,$11d0,$0f99
  dc.w $0d61,$b28,$8ee,$6b3,$478,$23c
  dc.w $10,$fdc4,$fb88,$f94d,$f712,$f518,$f29f  ;HIER!
  dc.w $f067,$ee30,$ebfa,$e9c6,$e794,$e563,$e335
  dc.w $e109,$dedf,$dcb8,$da94,$d872,$d654,$d439
  dc.w $d221,$d00d,$cdfd,$cbf0,$c9e8,$c7e3,$c5e4
  dc.w $c3e8,$c1f2,$c000,$be13,$bc2c,$ba49,$b86c
  dc.w $b695,$b4c3,$b3f8,$b132,$af72,$adb9,$ac06
  dc.w $aa5a,$a8b4,$a715,$a57e,$a3ed,$a263,$a0e1
  dc.w $9f66,$9df2,$9c86,$9b22,$99c6,$9872,$9726
  dc.w $95e2,$94a6,$9373,$9248,$9126,$900c,$8efc
  dc.w $8df3,$8cf4,$8bfe,$8b11,$8a2d,$8952,$8880
  dc.w $87b8,$86f9,$8644,$8598,$84f5,$845d,$83cd
  dc.w $8348,$82cc,$825a,$81f2,$8193,$813f,$80f4
  dc.w $80b4,$807d,$8050,$802d,$8014,$8005,$8000
  
  dc.w $8005,$8014,$802d,$8050,$807d,$80b4,$80f4
  dc.w $813f,$8193,$81f2,$825a,$82cc,$8348,$83cd
  dc.w $845d,$84f5,$8598,$8644,$86f9,$87b8,$8880
  dc.w $8952,$8a2d,$8b11,$8bfe,$8cf4,$8df3,$8efc
  dc.w $900c,$9126,$9248,$9373,$94a6,$95e2,$9726
  dc.w $9872,$99c6,$9b22,$9c86,$9df2,$9f66,$a0e1
  dc.w $a263,$a3ed,$a57e,$a715,$a8b4,$aa5a,$ac06
  dc.w $adb9,$af72,$b132,$b2f8,$b4c3,$b695,$b86c
  dc.w $ba49,$bc2c,$be13,$c000,$c1f2,$c3e8,$c5e4
  dc.w $c7e3,$c9e8,$cbf0,$cdfd,$d00d,$d221,$d439
  dc.w $d654,$d872,$da94,$dcb8,$dedf,$e109,$e335
  dc.w $e563,$e794,$e9c6,$ebfa,$ee30,$f067,$f29f
  dc.w $f4d3,$f712,$f94d,$fb88,$fdc4
  dc.w $10,$23c,$478,$6b3,$8ee,$b28,$d61,$f99,$11d0
  dc.w $1406,$163a,$186c,$1a9d,$1ccb,$1ef7,$2121
  dc.w $2348,$256c,$278e,$29ac,$2bc7,$2ddf,$2ff3
  dc.w $3203,$3410,$3618,$381d,$3a1c,$3c18,$3e0e
  dc.w $4000,$41ed,$43d4,$45b7,$4794,$496b,$4b3d
  dc.w $4d08,$4ece,$508e,$5247,$53fa,$55a6,$574c
  dc.w $58eb,$5a82,$5c13,$5d9d,$5f1f,$609a,$620e
  dc.w $637a,$64de,$663a,$678e,$68da,$6a1e,$6b5a
  dc.w $6c8d,$6db8,$6eda,$6ff4,$7104,$720d,$730c
  dc.w $7402,$74ef,$75d3,$76ae,$7780,$7848,$7907
  dc.w $79bc,$7a68,$7b0b,$7ba3,$7c33,$7cb8,$7d34
  dc.w $7da6,$7e0e,$7e6d,$7ec1,$7f0c,$7f4c,$7f83
  dc.w $7fb0,$7fd3,$7fec,$7ffb
fini:
