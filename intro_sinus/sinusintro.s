; vasmm68k_mot -kick1hunks -Fhunkexe -o ../../disk/sinus -nosym -no-opt sinusintro.s
 code_c $30000

openlib=-552
forbid=-132
permit=-138

startlist=38

x=$40
   
splane=$54000      
blane=splane+(50*260)     
blane2=blane+(50*260)
plane1=blane2+(50*260)
plane2=plane1+(50*260)
plane3=plane2+(50*260)
plane4=plane3+(50*260)
plane5=plane4+(50*260)


start:	
	move.l	#trap,$80
	trap	#0
	rts

trap:	
	movem.l d0-d7/a0-a6,-(a7)
	move #0,$dff180
	move.w #$03e0,$dff096
	lea splane,a0
clrlop:	
	clr.l (a0)+
	cmp.l #plane5,a0
	bne  clrlop

	bsr		makecl1
	bsr		makecl2
	move.l	#copperlist2,$dff080
	clr.w	$dff088

	move.w	#$2a71,$dff08e
	move.w	#$30c7,$dff090
	move.w	#$0018,$dff092	;$28
	move.w	#$00e0,$dff094	;$e0
	move.w	#%0101001000000000,$dff100
	move.w	#$0001,$dff102
	clr.w	$dff104
	move.w	#$0,$dff180
	move.w 	#$ffff,$dff182
	move.w	#0,$dff108	;84
	move.w	#0,$dff10a

	move.w	#$8030,$dff09a
	move.w	#$83c0,$dff096
	move.l	$6c,ipuf
	bsr		blitlogo
	bsr		setcolors


	jsr	mt_init


	move.l #irq,$6c

wait:	
	btst	#6,$bfe001
	bne.s	wait
	
wt:	btst	#14,$dff002
	bne.S	wt

	move.l ipuf,$6c

;alte copper-list !
	move.l #grname,a1
	clr.l d0
	move.l 4,a6
	jsr openlib(a6)
	move.l d0,a4
	move.l startlist(a4),$dff080
	clr.w $dff088
	move.w #$83e0,$dff096
	move.w #$f,$dff096

ende:	
	clr.l d0
	movem.l	(a7)+,d0-d7/a0-a6
	rte



irq:	
	move.l	d1,-(a7)
	move.w 	$dff01c,d1
	btst	#$000e,d1
	beq.s	irqret
	and.w	$dff01e,d1
	btst	#$0006,d1
	beq.s	skip1
irqret:	
	move.l	(a7)+,d1
	move.w	#$3fff,$dff09c
	rte

skip1:	btst	#$0004,d1
	beq.s	skip2
	
;	move.w	#$f00,$dff180
	movem.l d0-d7/a0-a6,-(a7)
	bsr	clr
	bsr	hscroll2
	bsr	scroll
	bsr	hide

	jsr	mt_music

	movem.l	(a7)+,d0-d7/a0-a6	
	move	#$000,$dff180
	bra.s	irqret
	
skip2:	
	bra.s	irqret




grname:	dc.b "graphics.library",0
	even

ipuf:	dc.l 0


cop:	dc.l	0
copperlist1:
	blk.w 4000
copperlist2:
	blk.w 10000



;*************************************
;******* PART 2: WOBBLE-SCROLL *******
;*************************************


fastcl:	
	lea	copperlist1,a0
	move.l	screen,d6
	swap	d6
	move.w	d6,2(a0)
	swap	d6
	move.w	d6,6(a0)

	move.l	screen,d6
	add.l	#50,d6
	swap	d6
	move.w	d6,10(a0)
	swap	d6
	move.w	d6,14(a0)

	lea	copperlist2,a0
	move.l	screen,d6
	swap	d6
	move.w	d6,2(a0)
	swap	d6
	move.w	d6,6(a0)

	move.l	screen,d6
	add.l	#50,d6
	swap	d6
	move.w	d6,10(a0)
	swap	d6
	move.w	d6,14(a0)
	rts


makecl1:
	lea	copperlist1,a0

	move.l	screen,d6
	move.w	#$00e0,(a0)+
	swap	d6
	move.w	d6,(a0)+
	move.w	#$00e2,(a0)+
	swap	d6
	move.w	d6,(a0)+

	move.l	screen,d6
	add.l	#50,d6
	move.w	#$00e4,(a0)+
	swap	d6
	move.w	d6,(a0)+
	move.w	#$00e6,(a0)+
	swap	d6
	move.w 	d6,(a0)+

	move.l #plane2+(50*x),d6
	move.w #$00e8,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00ea,(a0)+
	swap d6
	move.w d6,(a0)+

	move.l #plane3+(50*x),d6
	move.w #$00ec,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00ee,(a0)+
	swap d6
	move.w d6,(a0)+

	move.l #plane4+(50*x),d6
	move.w #$00f0,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00f2,(a0)+
	swap d6
	move.w d6,(a0)+


	lea	rasters,a1
	move.b	#$38,d0	

rlop:	
	move.b	d0,(a0)+
	move.b	#$0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1)+,(a0)+	

	addq.b	#1,d0
	cmp.b	#$6e,d0
	bne.s	rlop

	move.w	#$6f0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$100,(a0)+
	move.w	#%0101001000000000,(a0)+

	move.b	#$70,d0	

rlop2:	
	move.b	d0,(a0)+
	move.b	#$0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1)+,(a0)+	

	addq.b	#1,d0
	cmp.b	#$cf,d0
	bne.s	rlop2


	move.w	#$cfd7,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$9c,(a0)+
	move.w	#$8010,(a0)+

	move.b	#$d0,d0	

rlop3:	
	move.b	d0,(a0)+
	move.b	#$0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1)+,(a0)+	

	addq.b	#1,d0
	cmp.b	#$db,d0
	bne.s	rlop3


	move #$dbd7,(a0)+
	move #$fffe,(a0)+
	move #$100,(a0)+
	move.w #%0010001000000000,(a0)+

	move.b	#$dc,d0	

rlop4:	move.b	d0,(a0)+
	move.b	#$d7,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1)+,(a0)+	

	addq.b	#1,d0
	cmp.b	#$20,d0
	bne.s	rlop4

	rts




makecl2:
	lea	copperlist2,a0

	move.l	screen,d6
	move.w	#$00e0,(a0)+
	swap	d6
	move.w	d6,(a0)+
	move.w	#$00e2,(a0)+
	swap	d6
	move.w	d6,(a0)+

	move.l	screen,d6
	add.l	#50,d6
	move.w	#$00e4,(a0)+
	swap	d6
	move.w	d6,(a0)+
	move.w	#$00e6,(a0)+
	swap	d6
	move.w 	d6,(a0)+

	move.l #plane2+(50*x),d6
	move.w #$00e8,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00ea,(a0)+
	swap d6
	move.w d6,(a0)+

	move.l #plane3+(50*x),d6
	move.w #$00ec,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00ee,(a0)+
	swap d6
	move.w d6,(a0)+

	move.l #plane4+(50*x),d6
	move.w #$00f0,(a0)+
	swap d6
	move.w d6,(a0)+
	move.w #$00f2,(a0)+
	swap d6
	move.w d6,(a0)+


	lea	rasters,a1
	move.b	#$38,d0	

rlob:	
	move.b	d0,(a0)+
	move.b	#$0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$18e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$196,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$19e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1a6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1ae,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1b6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1be,(a0)+
	move.w	(a1)+,(a0)+	



	addq.b	#1,d0
	cmp.b	#$6e,d0
	bne.s	rlob

	move.w	#$6f0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$100,(a0)+
	move.w	#%0101001000000000,(a0)+

	move.b	#$70,d0	

rlob2:	
	move.b	d0,(a0)+
	move.b	#$0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$18e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$196,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$19e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1a6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1ae,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1b6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1be,(a0)+
	move.w	(a1)+,(a0)+	


	addq.b	#1,d0
	cmp.b	#$cf,d0
	bne.s	rlob2


	move.w	#$cfd7,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$9c,(a0)+
	move.w	#$8010,(a0)+

	move.b	#$d0,d0	

rlob3:	
	move.b	d0,(a0)+
	move.b	#$0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$18e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$196,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$19e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1a6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1ae,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1b6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1be,(a0)+
	move.w	(a1)+,(a0)+	


	addq.b	#1,d0
	cmp.b	#$db,d0
	bne.s	rlob3


	move #$dbd7,(a0)+
	move #$fffe,(a0)+
	move #$100,(a0)+
	move.w #%0010001000000000,(a0)+

	move.b	#$dc,d0	

rlob4:	
	move.b	d0,(a0)+
	move.b	#$d7,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$186,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$18e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$196,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$19e,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1a6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1ae,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1b6,(a0)+
	move.w	(a1),(a0)+	
	move.w	#$1be,(a0)+
	move.w	(a1)+,(a0)+	


	addq.b	#1,d0
	cmp.b	#$20,d0
	bne.s	rlob4

	rts



print2:	movem.l a0/a1,-(a7)

	move d1,d3
	move d2,d4
	lea c2,a0
	lea splane,a1
	ext.w d0
	sub.w #$20,d0
	mulu #128,d0
	add.l d0,a0

	mulu #50,d4
	add.l d4,a1
	move d3,d5
	lsr #3,d3
	ext.l d3
	add.l d3,a1
	and #15,d5
	lsl #6,d5
	lsl #6,d5

blop2:	
	btst	#14,$dff002
	bne blop2

	or.w #%0000110111111100,d5
	move.w d5,$dff040
	move.l a0,$dff050
	move.l a1,$dff04c
	move.l a1,$dff054
	move.w #0,$dff064
	move.w #46,$dff066
	move.w #46,$dff062

	move.w #16*64+2,$dff058

	movem.l (a7)+,a0/a1
	rts



scroll:	
	bsr		blit
	move.l	screen,d0
	cmp.l	#blane2,d0
	bne.S	sc1
	move.l	#blane,screen
	bra.S	sc2
sc1:	move.l	#blane2,screen	
sc2:	bsr		fastcl
	rts	


hscroll2:
	tst.w	stopcounter
	beq.s 	hwait2
	sub	#1,stopcounter
	rts

hwait2:	
	btst	#14,$dff002
	bne.S	hwait2
	move.w	#%0000100111110000,d7
	move.w 	#16,d6
	sub 	speed,d6
	lsl 	#4,d6
	lsl 	#8,d6
	or.w 	d6,d7
	move.w 	d7,$dff040
	move.l	#splane+4,$dff050
	move.l	#splane+2,$dff054
	move.w	#2,$dff066
	move.w	#2,$dff064
	move.w	#16*64+24,$dff058
	move.w 	sphase,d3
	sub.w	speed,d3
	move.w 	d3,sphase
	bpl.s	hend

	move.w	#384,d6
	add.w	sphase,d6
	add.w	#13,sphase
	move.l	textpoint,a5
back2:	
	move.b	(a5)+,d0
	move.l	a5,textpoint
	cmp.b   #2,d0
	beq.s	speedset
	cmp.b	#3,d0
	beq.s	spedset

	cmp.b 	#1,d0
	beq.s	stop
	tst.b	d0
	beq.s	res2	
	
	move	#0,d2
	move	d6,d1
	bsr		print2
hend:	rts
	
res2:   move.l	#text,a5
	bra.S	back2

stop:	move.w	#100,stopcounter
	rts
speedset:
	clr.l d0
	move.b	(a5)+,d0
	move.l	a5,textpoint
	move.w  d0,speeed
  	rts

spedset:clr.l d0
	move.b	(a5)+,d0
	move.l	a5,textpoint
	move.w  d0,speed
  	rts


stopcounter:
 dc.w 0
blit:	lea	postab2,a0

bback:		
	move.l	#splane+2,a1
	move.l	screen,d0
	add.l	#1054,d0		;Hier! (20mal)
	move.l	d0,ubloop1+2		;direkt zur�cksetzen
	move.l	d0,obloop1+2		;direkt zur�cksetzen

bw2:	btst	#14,$dff002
	bne.S	bw2

	move #%0000110111111100,$dff040
	move #$0,$dff042
	move #$ffff,$dff044
	move #48,$dff064
	move #48,$dff062	;HIER!
	move #48,$dff066	;HIER!

	
	cmp	#90,pos
	bge	oben

unten:	lea	sinusb,a6

	cmp	#88,pos
	bne.s	uskippo
	move.w	#92,pos

uskippo:
	move.w	#90,d5
	sub.w	pos,d5		
	add.w	d5,d5
	move.w	(a6,d5.w),d5

ubloop2:
	move.w	#%1100000000000000,d1
	
ubloop1:
	move.l	#blane,a2	
	move	(a0)+,d0

	sub	#100,d0	
	mulu	d5,d0
	lsr	#7,d0
	add	#100,d0

	mulu	#50,d0		;HIER!
	add.l	d0,a2

	move.w	d1,$dff046
	move.l	a1,$dff050
	move.l	a2,$dff04c
	move.l	a2,$dff054
	move.w	#15*64+1,$dff058	

	lsr	#2,d1
	bne	ubloop1

	addq.l	#2,a1
	addq.l	#2,ubloop1+2	;direkt erh�hen
	cmp.l	#splane+52,a1
	bne		ubloop2
	bra		bgo



oben:	
	lea	sinusb,a6

	cmp	#92,pos
	bne.s	oskippo
	move.w	#88,pos

oskippo:move.w	pos,d5
	sub.w	#90,d5		
	add.w	d5,d5
	move.w	(a6,d5.w),d5

obloop2:move.w	#%1110000000000000,d1
	
obloop1:
	move.l	#blane,a2	
	move	(a0)+,d0
	sub	#100,d0	

	mulu	d5,d0
	lsr.w	#7,d0
	move	#100,d7
	sub.w	d0,d7
	move.w	d7,d0
	
	mulu	#50,d0		;HIER!
	add.l	d0,a2

	move.w	d1,$dff046
	move.l	a1,$dff050
	move.l	a2,$dff04c
	move.l	a2,$dff054
	move.w	#15*64+1,$dff058	

	lsr	#2,d1
	bne	obloop1

	addq.l	#2,a1
	addq.l	#2,obloop1+2	;direkt erh�hen
	cmp.l	#splane+52,a1
	bne	obloop2
;	bra.s	bgo


bgo:	tst.w	posflag
	bne.S	ab
	move.w	speeed,d5
	add.w	d5,pos	
	cmp.w	#180,pos
	blt.S	b1
	move.w	#1,posflag
	eor.w	#1,sichtbar
b1:     bra.S	bend
ab:     move.w	speeed,d5
	sub.w	d5,pos
	cmp.w	#0,pos
	bpl.S	b1 	
	move.w	#0,posflag
	eor.w	#1,sichtbar
bend:	move.w	#$ffff,$dff044
	move.w	#$ffff,$dff046
	rts


speeed:	dc.w	5




posflag: dc.w 0
sichtbar: dc.w 0



clr:    move.l	screen,d0
	cmp.l	#blane,d0
	bne.s	cc1
	move.l	#blane,d0
	bra.s	cc2
cc1:	move.l	#blane2,d0	

cc2:	add.l	#1200,d0

cw:	btst	#14,$dff002
	bne.s	cw

	move.w	#%0000000111110000,$dff040
	move.l	d0,$dff054
	move.w	#0,$dff066
	move.w	#0,$dff074
	move.w	#220*64+25,$dff058
	rts



blitlogo:
	lea	logo+64-400,a0
	lea	plane2+6,a1

	move.w	#%0101100111110000,$dff040
	move.w	#0,$dff042
	move.l	a0,$dff050
	move.l	a1,$dff054
	move.w	#10,$dff066
	move.w	#0,$dff064
	move.w	#260*64+20,$dff058

bl:	btst	#14,$dff002
	bne.S	bl


	lea	logo+64-400+(40*256),a0
	lea	plane2+6+(50*260),a1

	move.w	#%0110100111110000,$dff040
	move.w	#0,$dff042
	move.l	a0,$dff050
	move.l	a1,$dff054
	move.w	#10,$dff066
	move.w	#0,$dff064
	move.w	#260*64+20,$dff058

bl2:	btst	#14,$dff002
	bne.S	bl2
	

	lea	logo+64-400+(2*40*256),a0
	lea	plane2+6+(2*50*260),a1

	move.w	#%0101100111110000,$dff040
	move.w	#0,$dff042
	move.l	a0,$dff050
	move.l	a1,$dff054
	move.w	#10,$dff066
	move.w	#0,$dff064
	move.w	#260*64+20,$dff058

bl3:	btst	#14,$dff002
	bne.S	bl3

	rts



hide:   tst.w	sichtbar
	bne.S	setcolors2

setcolors:
	move.w	#$000,$dff180
	move.w	#$777,$dff182
	move.w	#$ddd,$dff184
	lea	$dff188,a0
	lea	logo+2,a1
	move.w	#6,d4
setclop:
	move.w	(a1)+,(a0)+
	move.w	#$777,(a0)+
	move.w	#$ddd,(a0)+	
	move.w	#$fff,(a0)+
	dbra	d4,setclop

	move.l	#copperlist2,$dff080
	rts

setcolors2:
	move.w	#$000,$dff180
	move.w	#$777,$dff182
	move.w	#$ddd,$dff184
	lea	$dff188,a0
	lea	logo+2,a1
	move.w	#6,d4
setclop2:
	move.w	(a1),(a0)+
	move.w	(a1),(a0)+
	move.w	(a1),(a0)+
	move.w	(a1)+,(a0)+
	dbra	d4,setclop2

	move.l	#copperlist1,$dff080
	rts



speed: dc.w 3
sphase:	dc.w 0
scrolladr: dc.l 0

textpoint: dc.l text
;3=scrollspeed
;2=sinuspeed
;0=ende
;1=stehenbleiben
text:
 DC.b 3,6
 DC.b "   IT'S SUBWAY AGAIN !    ",1
 DC.b  3,3,"AND WE BRING YOU... ",3,6
 DC.b "      DARK FUSION +       ",1 
 DC.b  3,3,"CRACKED BY ",3,6
 DC.b "  MATHEW + TILT OF SUBWAY  ",1
 DC.b " ",3,4,"   THANX TO MIRAGE UK FOR THE ORIGINAL !   "
 DC.b "  PRESS RIGHT MOUSEBUTTON FOR UNLIMITED LIVES, LEFT ONE FOR NO "
 DC.b "TRAINER !     "
 DC.b "SUBWAY'S 'MERRY X-MAS' AND 'HAPPY NEW YEAR' GREETS FLY TO:      "
 DC.b  3,6,"   ---"
 DC.b "  VISION FACTORY (HI L. !, HI TSH !)  ---"
 DC.b "  UNIQUE (HI SAVAGE ! VERY NICE X-MAS DEMO !)  ---"
 DC.b "  UNICORN  ---"
 DC.b "  TSK CREW (HI COPYRIGHTHUNTER + CREEPING DEATH !) ---"
 DC.b "  TRISTAR  ---"
 DC.b "  TRILOGY  ---"
 DC.b "  TARKUS TEAM  (HI FFC + FTL !! HI MASON !)  ---" 
 DC.b "  SUPREME  ---"
 DC.b "  SPREADPOINT  ---"
 DC.b "  SPIRIT  ---"
 DC.b "  SILENTS (HI SKIPPY !)  ---"
 DC.b "  SCOOPEX (HI RANGER !)  ---"
 DC.b "  ROBOTECH (HI MAGIC SECTOR !)  ---"
 DC.b "  RIGOR MORTIS ---"
 DC.b "  REFLEX  ---"
 DC.b "  RED SECTOR  ---"
 DC.b "  REDS  ---"
 DC.b "  REBELS (NICE DEMOS !!)  ---"
 DC.b "  QUICKSILVER  ---"
 DC.b "  POWER DRIVE  ---"
 DC.b "  PORTUGUESE CONNECTION  ---"
 DC.b "  POISON  ---"
 DC.b "  PARANOIMIA (HI HENRIK!)  ---"
 DC.b "  PARADOX  ---"
 DC.b "  ORACLE (HI VERTIGO! HI WEETIBIX!)  ---"
 DC.b "  NO LIMITS  ---"
 DC.b "  NEW STYLE  ---"
 DC.b "  NEUTRON DANCE  ---"
 DC.b "  MIRAGE UK (HI TURBO !)  ---"
 DC.b "  MAGNETIC FIELDS (YOHO ZARCH, THANX FOR THIS NEAT SOUND !)  ---"
 DC.b "  JUNGLE COMMAND (HI ARTELLI !  HI JOHN !)  ---"
 DC.b "  JCS (SPAIN!!)  ---"
 DC.b "  IT  ---"
 DC.b "  IPEC ELITE (NICE TO SPEAK TO YOU, PORTA!)  ---"
 DC.b "  HORIZON  ---"
 DC.b "  HEADWAVE  ---"
 DC.b "  GOD (HI CLOUDHUNTER !)  ---"
 DC.b "  GUARDIANS OF DOOM  ---"
 DC.b "  FASTMEM  ---"
 DC.b "  FAIRLIGHT (HI ZIKE ! COOLEST 3-WAYS)  ---"
 DC.b "  EXXON (HI TOM !)  ---"
 DC.b "  EXODUS  ---"
 DC.b "  ENERGY (HI FLOYD, Q-BACK + JASON !)  ---"
 DC.b "  ECSTASY (HI BIG AL !)  ---"
 DC.b "  DRAGONS  (HI MEGABLAST !)  ---"
 DC.b "  DISKNET  ---"
 DC.b "  DIGITAL FORCE  (HI JANN !)  ---"
 DC.b "  CYBORGS  ---"
 DC.b "  BYTE BUSTERS  ---"
 DC.b "  BLACK MONKS  (SPECIAL HI TO GREG+JAMMASTER !)  ---"
 DC.b "  B52'S (HI SIR !)  ---"
 DC.b "  AUTOMATION (SLAYER) ---"
 DC.b "  ATOMIX  ---" 
 DC.b "  ARCADIA  ---"
 DC.b  3,6,"                                                  ",0
      

even



rasters:
	dc.w	$0cf,$1cf,$2cf,$3cf,$4cf,$0df,$1df,$2df,$3df,$4df
	dc.w	$0ef,$1ef,$2ef,$3ef,$4ef,$0ff,$1ff,$2ff,$3ff,$4ff
	dc.w	$4ff,$3ff,$2ff,$1ff,$0ff,$4ef,$3ef,$2ef,$1ef,$0ef
	dc.w	$4df,$3df,$2df,$1df,$0df,$4cf,$3cf,$2cf,$1cf,$0cf
	dc.w	$4bf,$3bf,$2bf,$1bf,$0bf,$4af,$3af,$2af,$1af,$0af
	dc.w	$49f,$39f,$29f,$19f,$09f,$48f,$38f,$28f,$18f,$08f
	dc.w	$47f,$37f,$27f,$17f,$07f,$46f,$36f,$26f,$16f,$06f
	dc.w	$45f,$35f,$25f,$15f,$05f,$44f,$34f,$24f,$14f,$04f
	dc.w	$08f,$18f,$28f,$38f,$48f,$09f,$19f,$29f,$39f,$49f
	dc.w	$0af,$1af,$2af,$3af,$4af,$0bf,$1bf,$2bf,$3bf,$4bf
	dc.w	$0cf,$1cf,$2cf,$3cf,$4cf,$0df,$1df,$2df,$3df,$4df
	dc.w	$0ef,$1ef,$2ef,$3ef,$4ef,$0ff,$1ff,$2ff,$3ff,$4ff
	dc.w	$4ff,$3ff,$2ff,$1ff,$0ff,$4ef,$3ef,$2ef,$1ef,$0ef
	dc.w	$4df,$3df,$2df,$1df,$0df,$4cf,$3cf,$2cf,$1cf,$0cf
	dc.w	$4bf,$3bf,$2bf,$1bf,$0bf,$4af,$3af,$2af,$1af,$0af
	dc.w	$49f,$39f,$29f,$19f,$09f,$48f,$38f,$28f,$18f,$08f
	dc.w	$47f,$37f,$27f,$17f,$07f,$46f,$36f,$26f,$16f,$06f
	dc.w	$06f,$16f,$26f,$36f,$46f,$07f,$17f,$27f,$37f,$47f
	dc.w	$08f,$18f,$28f,$38f,$48f,$09f,$19f,$29f,$39f,$49f
	dc.w	$0af,$1af,$2af,$3af,$4af,$0bf,$1bf,$2bf,$3bf,$4bf
	dc.w	$0cf,$1cf,$2cf,$3cf,$4cf,$0df,$1df,$2df,$3df,$4df
	dc.w	$0ef,$1ef,$2ef,$3ef,$4ef,$0ff,$1ff,$2ff,$3ff,$4ff


pos:	dc.w 0
postab2: incbin "sinus.intro"

;blk.w 2000,0

screen:	dc.l blane2
c2: incbin "char.intro"	
;blk.b 8192,0
sinusb:	incbin "sinusc"
;blk.b 500
logo:	incbin "goofylogo"
;blk.b 41024


; -----------------------------------------------------
; ------- D.O.C SoundTracker V2.0 - playroutine -------
; -----------------------------------------------------
; ---- Improved and omptimized by Unknown of D.O.C ----
; --------- Based on the playroutine from TJC ---------
; -----------------------------------------------------
; ------ Adapted for SondTracker V2.3 Songs by  -------
; ----------- M.A.S.H. and Mathew of Subway. ----------
; -----------------------------------------------------      


 blk.w 	10,0

mt_init:
	move.l	mt_point,a0
	move.b	$1d7(a0),d0
	and.b	#$f0,d0
	cmp.b	#$70,d0
	bne.s	Version2_3
	lea	LabelTab2_0,a0
	bra.s	No2_3
Version2_3:
	lea	LabelTab2_3,a0
No2_3:	lea	AdressTab,a1
ChgL:	move.l	(a1)+,a2
	move.w	(a0)+,(a2)
	cmp.l	#-1,(a1)
	bne.s	ChgL
	move.l	mt_point,a0
Not2_3:	add.l	#$03b8,a0
	moveq	#$7f,d0				;$80 ?????
	moveq	#0,d1
mt_init1:
	move.l	d1,d2
	subq.w	#1,d0
mt_init2:
	move.b	(a0)+,d1
	cmp.b	d2,d1
	bgt.s	mt_init1
	dbf	d0,mt_init2
	addq.b	#1,d2

mt_init3:
	move.l	mt_point,a0
	lea	mt_sample1(pc),a1
	asl.l	#8,d2
	asl.l	#2,d2
mt_chg1: 
	add.l	#$438,d2
	add.l	a0,d2
mt_chg2: 
	moveq	#$1e,d0
mt_init4:
	move.l	d2,(a1)+
	moveq	#0,d1
	move.w	42(a0),d1
	asl.l	#1,d1
	add.l	d1,d2
	add.l	#$1e,a0
	dbf	d0,mt_init4

	lea	mt_sample1(PC),a0
	moveq	#0,d0
mt_clear:
	move.l	(a0,d0.w),a1
	clr.l	(a1)
	addq.w	#4,d0
mt_chg3:
	cmp.w	#$7c,d0
	bne.s	mt_clear

	clr.w	$dff0a8
	clr.w	$dff0b8
	clr.w	$dff0c8
	clr.w	$dff0d8
	clr.l	mt_partnrplay
	clr.l	mt_partnote
	clr.l	mt_partpoint
	
	move.l	a0,-(a7)
	move.l	mt_point,a0
mt_chg4:
	add.l	#$3b6,a0
	move.b	(a0),mt_maxpart+1
	move.l	(a7)+,a0

	rts

mt_end:	
	clr.w	$dff0a8
	clr.w	$dff0b8
	clr.w	$dff0c8
	clr.w	$dff0d8
	move.w	#$f,$dff096
	rts

mt_music:
	movem.l	d0-d7/a0-a6,-(a7)
	addq.w	#1,mt_counter
mt_cool:
	cmp.w	#6,mt_counter
	beq.s	mt_six
	bsr.s	mt_notsix
	bra.s	mt_end2
mt_six:	clr.w	mt_counter
	bsr 	mt_rout2
mt_end2:
	movem.l	(a7)+,d0-d7/a0-a6
	rts

mt_notsix:
	lea	mt_aud1temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp1
	lea	$dff0a0,a5		
	bsr.s	mt_arprout
mt_arp1:
	lea	mt_aud2temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp2
	lea	$dff0b0,a5
	bsr.s	mt_arprout
mt_arp2:
	lea	mt_aud3temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp3
	lea	$dff0c0,a5
	bsr.s	mt_arprout
mt_arp3:
	lea	mt_aud4temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp4
	lea	$dff0d0,a5
	bra.s	mt_arprout
mt_arp4:
	rts

mt_arprout:
	move.b	2(a6),d0
	and.b	#$0f,d0
	tst.b	d0
	beq	mt_arpegrt
	cmp.b	#$01,d0
	beq.s	mt_portup
	cmp.b	#$02,d0
	beq.s	mt_portdwn
	cmp.b	#$0a,d0
	beq.s	mt_volslide
	rts

mt_portup:
	moveq	#0,d0
	move.b	3(a6),d0
	sub.w	d0,22(a6)
	cmp.w	#$71,22(a6)
	bpl.s	mt_ok1
	move.w	#$71,22(a6)
mt_ok1:	move.w	22(a6),6(a5)
	rts

mt_portdwn:
	moveq	#0,d0
	move.b	3(a6),d0
	add.w	d0,22(a6)
mt_chg5:
	cmp.w	#$538,22(a6)
	bmi.s	mt_ok2
mt_chg6:move.w	#$538,22(a6)
mt_ok2:	move.w	22(a6),6(a5)
	rts

mt_volslide:
	moveq	#0,d0
	move.b	3(a6),d0
	lsr.b	#4,d0
	tst.b	d0
	beq.s	mt_voldwn
	add.w	d0,18(a6)
	cmp.w	#64,18(a6)
	bmi.s	mt_ok3
	move.w	#64,18(a6)
mt_ok3:	move.w	18(a6),8(a5)
	rts
mt_voldwn:
	moveq	#0,d0
	move.b	3(a6),d0
	and.b	#$0f,d0
	sub.w	d0,18(a6)
	bpl.s	mt_ok4
	clr.w	18(a6)
mt_ok4:	move.w	18(a6),8(a5)
	rts

mt_arpegrt:
	move.w	mt_counter(PC),d0
	cmp.w	#1,d0
	beq.s	mt_loop2
	cmp.w	#2,d0
	beq.s	mt_loop3
	cmp.w	#3,d0
	beq.s	mt_loop4
	cmp.w	#4,d0
	beq.s	mt_loop2
	cmp.w	#5,d0
	beq.s	mt_loop3
	rts

mt_loop2:
	moveq	#0,d0
	move.b	3(a6),d0
	lsr.b	#4,d0
	bra.s	mt_cont
mt_loop3:
	moveq	#$00,d0
	move.b	3(a6),d0
	and.b	#$0f,d0
	bra.s	mt_cont
mt_loop4:
	move.w	16(a6),d2
	bra.s	mt_endpart
mt_cont:
	add.w	d0,d0
	moveq	#0,d1
	move.w	16(a6),d1
	lea	mt_arpeggio(PC),a0
mt_loop5:
	move.w	(a0,d0),d2
	cmp.w	(a0),d1
	beq.s	mt_endpart
	addq.l	#2,a0
	bra.s	mt_loop5
mt_endpart:
	move.w	d2,6(a5)
	rts

mt_rout2:
	move.l	mt_point,a0
	move.l	a0,a3
	add.l	#$0c,a3
	move.l	a0,a2
mt_chg7:
	add.l	#$3b8,a2
mt_chg8:
	add.l	#$43c,a0
	move.l	mt_partnrplay(PC),d0
	moveq	#0,d1
	move.b	(a2,d0),d1
	asl.l	#8,d1
	asl.l	#2,d1
	add.l	mt_partnote(PC),d1
	move.l	d1,mt_partpoint
	clr.w	mt_dmacon

	lea	volumes,a4
	lea	$dff0a0,a5
	lea	mt_aud1temp(PC),a6
	bsr	mt_playit
	lea	$dff0b0,a5
	lea	mt_aud2temp(PC),a6
	bsr	mt_playit
	lea	$dff0c0,a5
	lea	mt_aud3temp(PC),a6
	bsr	mt_playit
	lea	$dff0d0,a5
	lea	mt_aud4temp(PC),a6
	bsr	mt_playit
	move.w	#$01f4,d0
mt_rls:	dbf	d0,mt_rls

	move.w	#$8000,d0
	or.w	mt_dmacon,d0
	move.w	d0,$dff096

	lea	mt_aud4temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice3
	move.l	10(a6),$dff0d0
	move.w	#1,$dff0d4
mt_voice3:
	lea	mt_aud3temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice2
	move.l	10(a6),$dff0c0
	move.w	#1,$dff0c4
mt_voice2:
	lea	mt_aud2temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice1
	move.l	10(a6),$dff0b0
	move.w	#1,$dff0b4
mt_voice1:
	lea	mt_aud1temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice0
	move.l	10(a6),$dff0a0
	move.w	#1,$dff0a4
mt_voice0:
	move.l	mt_partnote(PC),d0
	add.l	#$10,d0
	move.l	d0,mt_partnote
	cmp.l	#$400,d0
	bne.s	mt_stop
mt_higher:
	clr.l	mt_partnote
	addq.l	#1,mt_partnrplay
	moveq	#0,d0
	move.w	mt_maxpart(PC),d0
	move.l	mt_partnrplay(PC),d1
	cmp.l	d0,d1
	bne.s	mt_stop
	clr.l	mt_partnrplay

mt_stop:tst.w	mt_status
	beq.s	mt_stop2
	clr.w	mt_status
	bra.s	mt_higher
mt_stop2:
	rts

mt_playit:
	move.l	(a0,d1.l),(a6)
	addq.l	#4,d1
	moveq	#0,d2
	move.b	2(a6),d2
	and.b	#$f0,d2
	lsr.b	#4,d2

	move.b	(a6),d0
	and.b	#$f0,d0
	or.b	d0,d2
	tst.b	d2
	beq.s	mt_nosamplechange

	moveq	#0,d3
	lea	mt_samples(PC),a1
	move.l	d2,d4
	asl.l	#2,d2
	mulu	#$1e,d4
	move.l	(a1,d2),4(a6)
	move.w	(a3,d4.l),8(a6)
	move.w	2(a3,d4.l),18(a6)
	move.w	4(a3,d4.l),d3
	tst.w	d3
	beq.s	mt_displace
	move.l	4(a6),d2
	add.l	d3,d2
	move.l	d2,4(a6)
	move.l	d2,10(a6)
	move.w	6(a3,d4.l),8(a6)
	move.w	6(a3,d4.l),14(a6)
	move.w	18(a6),8(a5)
	bra.s	mt_nosamplechange

mt_displace:
	move.l	4(a6),d2
	add.l	d3,d2
	move.l	d2,10(a6)
	move.w	6(a3,d4.l),14(a6)
	move.w	18(a6),8(a5)
mt_nosamplechange:
	tst.w	(a6)
	beq.s	mt_retrout
	move.w	(a6),16(a6)
	move.w	20(a6),$dff096
	move.l	4(a6),(a5)
	move.w	8(a6),4(a5)
	move.w	(a6),6(a5)
	move.w	20(a6),d0
	or.w	d0,mt_dmacon
	move.w	18(a6),(a4)+

mt_retrout:
	tst.w	(a6)
	beq.s	mt_nonewper
	move.w	(a6),22(a6)

mt_nonewper:
	move.b	2(a6),d0
	and.b	#$0f,d0
	cmp.b	#$0b,d0
	beq.s	mt_posjmp
	cmp.b	#$0c,d0
	beq.s	mt_setvol
	cmp.b	#$0d,d0
	beq.s	mt_break
	cmp.b	#$0e,d0
	beq.s	mt_setfil
	cmp.b	#$0f,d0
	beq.s	mt_setspeed
	rts

mt_posjmp:
	not.w	mt_status
	moveq	#0,d0
	move.b	3(a6),d0
	subq.b	#1,d0
	move.l	d0,mt_partnrplay
	rts

mt_setvol:
	move.b	3(a6),8(a5)
	rts

mt_break:
	not.w	mt_status
	rts

mt_setfil:
	moveq	#0,d0
	move.b	3(a6),d0
	and.b	#1,d0
	rol.b	#1,d0
	and.b	#$fd,$bfe001
	or.b	d0,$bfe001
	rts

mt_setspeed:
	move.b	3(a6),d0
	and.b	#$0f,d0
	beq.s	mt_back
	clr.w	mt_counter
	move.b	d0,mt_cool+3
mt_back:rts

mt_aud1temp:
	blk.w	10,0
	dc.w	1
	blk.w	2,0
mt_aud2temp:
	blk.w	10,0
	dc.w	2
	blk.w	2,0
mt_aud3temp:
	blk.w	10,0
	dc.w	4
	blk.w	2,0
mt_aud4temp:
	blk.w	10,0
	dc.w	8
	blk.w	2,0

mt_partnote:	dc.l	0
mt_partnrplay:	dc.l	0
mt_counter:	dc.w	0
mt_partpoint:	dc.l	0
mt_samples:	dc.l	0
mt_sample1:	blk.l	31,0
mt_maxpart:	dc.w	0
mt_dmacon:	dc.w	0
mt_status:	dc.w	0

mt_arpeggio:
	dc.w $0358,$0328,$02fa,$02d0,$02a6,$0280,$025c
	dc.w $023a,$021a,$01fc,$01e0,$01c5,$01ac,$0194,$017d
	dc.w $0168,$0153,$0140,$012e,$011d,$010d,$00fe,$00f0
	dc.w $00e2,$00d6,$00ca,$00be,$00b4,$00aa,$00a0,$0097
	dc.w $008f,$0087,$007f,$0078,$0071,$0000,$0000,$0000

LabelTab2_0:
	dc.w	$1d8
	dc.w	$258
	dc.w	$700e
	dc.w	$003c
	dc.w	$1d6
	dc.w	$358
	dc.w	$358
	dc.w	$1d8
	dc.w	$258

LabelTab2_3:
	dc.w	$3b8
	dc.w	$438
	dc.w	$701e
	dc.w	$007c
	dc.w	$3b6
	dc.w	$538
	dc.w	$538
	dc.w	$3b8
	dc.w	$43c

AdressTab:
	dc.l	Not2_3+4
	dc.l	mt_chg1+4
	dc.l	mt_chg2
	dc.l	mt_chg3+2
	dc.l	mt_chg4+4
	dc.l	mt_chg5+2
	dc.l	mt_chg6+2
	dc.l	mt_chg7+4
	dc.l	mt_chg8+4
	dc.l	-1

mt_point:	
	dc.l	mt_data		;Adresse der Sounddaten.

volumes:
	dc.l	0,0,0,0		;Lautst�rke der vier Kan�le.

mt_data: incbin "rainbow"

;	blk.b	57000



fini:
