;vasmm68k_mot -kick1hunks -Fhunkexe -o ../../disk/intro -nosym -no-opt intro.s
; attention -nop-opt neeeded for sound tracker playing routine (self modifying code)


;    org	$30000
    code_c	$30000
;	load	$30000

	bra.s	start

; winkel:
; $1d1 - $048
; $0b8 - $12f

; LINESCROLL
; scrollt mit linienmodus...
; 06.09.90/Mathew

;planebreite	=	352	( = 22 words )
;planehoehe	=	352

scrollplane	=	$42000

planesize	=	400*44
plane1		=	scrollplane+(15*46)
plane2		=	plane1+planesize
planeend	=	plane2+planesize

;>extern "font2",char
;>extern "sincos$8000",sinustab
;>extern "logo",logo
;>extern "mod.popcorn",mt_mem

scrollspeed:	dc.w	8
rotatespeed:	dc.w	4
direction:	dc.w	-1
scrollwait:	dc.w	0


BLITWAIT:	MACRO
\@:		btst	#6,$002(a6)
		bne.s	\@
		ENDM


;------------------------------------------------ HAUPTPROGRAMM
start:	lea	trap(pc),a0			;
	move.l	a0,$80.w			;
	trap	#0				;
	rts					;


trap:	bsr	init				; initialisierungen
    	bsr	mt_init

rasterwait:
	move.l	$04(a6),d0
	and.l	#$0001ff00,d0
	cmp.l	#$00000a00,d0
	bne.s	rasterwait	

;	move.w	#$f00,$180(a6)
	bsr	clearscreen
	bsr	scroll
	bsr	linecopy
	bsr	changeplanes
	bsr	colorscroll
	bsr	mt_music
	move.w	#$000,$180(a6)

	bsr	transformscroll
	bsr	movescroll

w:	btst	#10,$dff016
	beq.s	w

	btst	#6,$bfe001
	bne.s	rasterwait

	bsr	mt_end
	bsr	exit
	rte





;------------------------------------------------ initialisierungen
;
init:	lea	$dff000,a6
	move.w	#$4000,$9a(a6)			; interrupts aus
	move.w	#$7fff,d0			;
	move.w	$1c(a6),d1			; intena retten
	move.w	d0,$9a(a6)			; alle interrupts aus
	or.w	#$c000,d1			;
	move.w	d1,intena			;
	bsr	wait_blank			;
	move.w	$02(a6),d1			; dmacon retten
	move.w	d0,$96(a6)			; dma aus
	or.w	#$8000,d1			;
	move.w	d1,dmacon			;

	lea	scrollplane-2,a0
clrlop:	clr.l	(a0)+
	cmp.l	#planeend,a0
	bne.s	clrlop

	move.w	#$3081,$08e(a6)			; screeneinstellungen
	move.w	#$30c1,$090(a6)			;
	move.w	#$0038,$092(a6)			;
	move.w	#$00d0,$094(a6)			;
	move.w	#$2200,$100(a6)			; eine bitplane
	move.w	#$0001,$102(a6)			;
	move.w	#$0000,$104(a6)			;
	move.w	#$0004,$108(a6)			;
	move.w	#$0004,$10a(a6)			;


	bsr	makecopperlist			; copperlist aufbauen
	bsr	makecolorlist			; colorlist aufbauen
	move.l	#copperlist,$84(a6)		; copperlist

	move.w	#$83c0,$96(a6)			; dmas ein

	move.l	$6c.w,ipuf			; interrupts stellen
	move.l	#interrupt,$6c.w		;
	move.w	#$c020,$9a(a6)			;

	clr.w	$8a(a6)				; aktivieren
	rts



;------------------------------------------------ zur�ck ins
;						  betriebssystem
exit:	move.w	#$7fff,d0			;
	move.w	d0,$96(a6)			; dma aus
	move.w	d0,$9a(a6)			; interrupts aus

	move.l	ipuf,$6c.w			; 
	clr.w	$80(a6)				; alte copperlist

	move.w	dmacon,$96(a6)			; dmacon retten
	move.w	intena,$9a(a6)			; intena retten
	rts



;------------------------------------------------ auf vertical blank
;						  warten

wait_blank:
	move.w	#$0020,$9c(a6)
wbl_loop:
	btst	#5,$1f(a6)
	beq.s	wbl_loop
	rts



;------------------------------------------------ interruptroutinen
;
interrupt:
	movem.l	d0-d7/a0-a5,-(a7)
	move.w	$1c(a6),d1
	btst	#$000e,d1
	beq 	intende
	and.w	$1e(a6),d1
	btst	#$0005,d1
	beq 	intende

verticalblank:

intende:
	movem.l (a7)+,d0-d7/a0-a5
	move.w	#$7fff,$9c(a6)
	rte


;------------------------------------------------ doublebuffering
;
changeplanes:
	move.l	frontplane(pc),d0
	move.l	backplane(pc),frontplane
	move.l	d0,backplane

;------------------------------------------------ copperlist aufbauen
;						  (planes eintragen)
makecopperlist:	 
	lea	copperplanes(pc),a0
	lea	copperplanes2(pc),a1
	lea	copperplanes3(pc),a2
	move.l	frontplane(pc),d6
	add.l	#(44*60)+2,d6

	move.w	d6,6(a0)
	swap	d6
	move.w	d6,2(a0)
	swap	d6
	add.l	#44,d6
	move.w	d6,14(a0)
	swap	d6
	move.w	d6,10(a0)
	swap	d6

	add.l	#44*2,d6
	move.w	d6,6(a1)
	swap	d6
	move.w	d6,2(a1)
	swap	d6
	add.l	#44,d6
	move.w	d6,14(a1)
	swap	d6
	move.w	d6,10(a1)
	swap	d6

	add.l	#44*41,d6
	move.w	d6,6(a2)
	swap	d6
	move.w	d6,2(a2)
	swap	d6
	add.l	#44,d6
	move.w	d6,14(a2)
	swap	d6
	move.w	d6,10(a2)
	swap	d6

	lea	logoplanes(pc),a0
	move.l	#logo+2,d6
	move.w	d6,6(a0)
	swap	d6
	move.w	d6,2(a0)
	move.l	#logo+1848+2,d6
	move.w	d6,14(a0)
	swap	d6
	move.w	d6,10(a0)
	move.l	#logo+1848+1848+2,d6
	move.w	d6,22(a0)
	swap	d6
	move.w	d6,18(a0)		
	rts


;------------------------------------------------ colorlist in der
;						  copperlist aufbauen
makecolorlist:
	lea	colorlist(pc),a0
	lea	colors(pc),a1
	move.b	#$38,d0
mclloop:move.b	d0,(a0)+
	move.b	#$0f,(a0)+
	move.w	#$fffe,(a0)+
	move.w	#$018e,(a0)+
	move.w	(a1)+,(a0)+
	addq.b	#1,d0
	cmp.b	#$59,d0
	bne.s	mclloop

	lea	colorlist2(pc),a0
	lea	colors(pc),a1
	move.w	#59,d0
mclloop2:
	move.w	#$180,(a0)+
	move.w	(a1)+,(a0)+
	dbra	d0,mclloop2

	lea	colorlist3(pc),a0
	lea	colors+45+65(pc),a1
	move.w	#59,d0
mclloop3:
	move.w	#$180,(a0)+
	move.w	-(a1),(a0)+
	dbra	d0,mclloop3

	rts


;------------------------------------------------ colors scrollen
;
colorscroll:
	lea	colors(pc),a0
	move.w	colorscrollpos,d0
	add.w	d0,d0
	add.w	d0,a0

	BLITWAIT
	move.w	#%0000100111110000,$40(a6)
	move.w	#$00,$42(a6)
	move.l	#-1,$44(a6)
	move.l	a0,$50(a6)
	move.l	#colorlist+6,$54(a6)
	move.w	#$00,$64(a6)
	move.w	#$06,$66(a6)
	move.w	#64*33+1,$58(a6)

	lea	colorlist2(pc),a1
	move.b	-3(a1),d0
	cmp.b	#$0f,d0
	beq.s	noscroll
	move.b	#$0f,-3(a1)	
		
	BLITWAIT
	move.l	a0,$50(a6)
	move.l	#colorlist2+2,$54(a6)
	move.w	#$00,$64(a6)
	move.w	#$02,$66(a6)
	move.w	#64*60+1,$58(a6)
	bra.s	nsskip
	
noscroll:
	move.b	#$11,-3(a1)

nsskip:	lea	colorlist3(pc),a1
	move.b	-3(a1),d0
	cmp.b	#$11,d0
	beq.s	noscroll2
	move.b	#$11,-3(a1)

	add.w	#110,a0	

	BLITWAIT
	move.l	a0,$50(a6)
	move.l	#colorlist3+2,$54(a6)
	move.w	#-4,$64(a6)
	move.w	#$02,$66(a6)
	move.w	#64*60+1,$58(a6)
	bra.s	nsskip2	

noscroll2:
	move.b	#$0f,-3(a1)

nsskip2:addq.w	#1,colorscrollpos
	cmp.w	#45,colorscrollpos
	bne.s	csskip
	move.w	#0,colorscrollpos

csskip:
	rts


;------------------------------------------------ scroller drehen
;
transformscroll:
	lea	linetab(pc),a0
	lea	linetab2(pc),a1
	lea	sinustab(pc),a2			; sinustabelle
	lea	256(a2),a3			; cosinustabelle
	
	move.w	winkel,d7
	add.w	d7,d7
	move.w	(a2,d7.w),d4			; sin(w)
	move.w	(a3,d7.w),d5			; cos(w)

	move.w	#12,d2
tsloop:
	movem.w	(a0),d0/d1
	muls	d5,d0				; cos(w) * x	
	muls	d4,d1				; sin(w) * y
	sub.l	d1,d0
	add.l	d0,d0
	swap	d0
	move.w	d0,(a1)+
	movem.w	(a0)+,d0/d1
	muls	d4,d0				; sin(w) * x
	muls	d5,d1				; cos(w) * y
	add.l	d0,d1
	add.l	d1,d1
	swap	d1
	move.w	d1,(a1)+

	movem.w	(a0),d0/d1	
	muls	d5,d0				; cos(w) * x	
	muls	d4,d1				; sin(w) * y
	sub.l	d1,d0
	add.l	d0,d0
	swap	d0
	move.w	d0,(a1)+
	movem.w	(a0)+,d0/d1
	muls	d4,d0				; sin(w) * x
	muls	d5,d1				; cos(w) * y
	add.l	d0,d1
	add.l	d1,d1
	swap	d1
	move.w	d1,(a1)+

	dbra	d2,tsloop
	rts



;------------------------------------------------ scroller bewegen
;
movescroll:
	tst.w	direction
	bne.s	skip1
	move.w	rotatespeed,d0
	sub.w	d0,winkel
	and.w	#$1ff,winkel
	rts

skip1:	move.w	rotatespeed,d0
	add.w	d0,winkel
	and.w	#$1ff,winkel
	rts




;------------------------------------------------ screen l�schen
;
clearscreen:
	move.l	backplane(pc),d0
	add.l	#(44*46),d0
	BLITWAIT
	move.w	#%0000000100000000,$40(a6)
	move.w	#$00,$42(a6)
	move.l	#-1,$44(a6)
	move.l	d0,$54(a6)
	move.w	#$00,$66(a6)
	move.w	#260*64+23,$58(a6)
	rts



;------------------------------------------------ zeichen ausgeben
;in:	d0.w ... char
;	d1.w ... x-position
;	d2.w ... y-position
;
print2:	lea	chartable(pc),a0
	sub.w	#$20,d0
	move.b	(a0,d0.w),d0
	lsl.w	#5,d0
	lea	char(pc),a0
	add.w	d0,a0

	move.l	backplane,a1
	mulu	#40,d2
	add.w	d2,a1
	move.w	d1,d2
	lsr.w	#4,d1
	add.w	d1,d1
	add.w	d1,a1
	and.w	#15,d2
	ror.w	#4,d2

	or.w	#%0000110111111100,d2
	BLITWAIT
	move.w	d2,$40(a6)
	move.w	#$00,$42(a6)
	move.l	#$ffff0000,$44(a6)
	move.l	a1,$4c(a6)
	move.l	a0,$50(a6)
	move.l	a1,$54(a6)
	move.w	#36,$62(a6)
	move.w	#-2,$64(a6)
	move.w	#36,$66(a6)
	move.w	#15*64+2,$58(a6)	
	rts


;------------------------------------------------ print f�r scroller
;						  (imeer selbe pos.)
print:	lea	chartable(pc),a0		;
	sub.w	#$20,d0
	move.b	(a0,d0.w),d0
	lsl.w	#5,d0
	lea	char(pc),a0
	add.w	d0,a0

	lea	scrollplane+44,a1
	BLITWAIT
	move.w	(a0)+,00*46(a1)
	move.w	(a0)+,01*46(a1)
	move.w	(a0)+,02*46(a1)
	move.w	(a0)+,03*46(a1)
	move.w	(a0)+,04*46(a1)
	move.w	(a0)+,05*46(a1)
	move.w	(a0)+,06*46(a1)
	move.w	(a0)+,07*46(a1)
	move.w	(a0)+,08*46(a1)
	move.w	(a0)+,09*46(a1)
	move.w	(a0)+,10*46(a1)
	move.w	(a0)+,11*46(a1)
	move.w	(a0)+,12*46(a1)
	move.w	(a0)+,13*46(a1)
	move.w	(a0)+,14*46(a1)
	move.w	(a0)+,15*46(a1)
scrollend
	rts


;------------------------------------------------ scrollen
;
scroll:
	tst.w	scrollwait
	beq.s	scrollskip

	subq.w	#1,scrollwait
	bra.s	scrollend

scrollskip:
	move.w	scrollspeed,d0
	neg.w	d0
	and.w	#$0f,d0
	ror.w	#4,d0
	or.w	#%0000100111110000,d0

	BLITWAIT
	move.w	d0,$40(a6)
	move.w	#$00,$42(a6)
	move.l	#-1,$44(a6)
	move.l	#scrollplane,$50(a6)
	move.l	#scrollplane-2,$54(a6)
	move.w	#0,$64(a6)
	move.w	#0,$66(a6)
	move.w	#15*64+23,$58(a6)

	move.w	scrollspeed,d0
	add.w	d0,scrollpos
	cmp.w	#16,scrollpos
	blt.s	nonewchar

	sub.w	#16,scrollpos
nextchar:
	move.l	textpointer(pc),a0
	moveq	#0,d0
back:	move.b	(a0)+,d0
	bne.s	notextend
	move.l	#text,textpointer
	bra.s	nextchar
	
notextend:
	cmp.b	#1,d0
	beq.s	changedirection
	cmp.b	#2,d0
	beq.s	changerotatespeed
	cmp.b	#3,d0
	beq.s	changescrollspeed
	cmp.b	#4,d0
	beq.s	stopscroller
	cmp.b	#5,d0
	beq.s	changewinkel
	move.l	a0,textpointer
	bsr	print

nonewchar:
	rts

changedirection:
	not.w	direction
	bra.s	back

changerotatespeed:
	move.b	(a0)+,d0
	move.w	d0,rotatespeed
	bra.s	back

changescrollspeed:
	move.b	(a0)+,d0
	move.w	d0,scrollspeed
	bra.s	back

stopscroller:
	move.b	(a0)+,d0
	move.w	d0,scrollwait
	bra.s	back

changewinkel:
	move.b	(a0)+,d0
	move.w	d0,winkel
	bra.s	back


;------------------------------------------------ scrollplane in plane
;						  kopieren
copy:	BLITWAIT
	move.w	#%0000100111110000,$40(a6)
	move.w	#$00,$42(a6)
	move.l	#-1,$44(a6)
	move.l	#scrollplane,$50(a6)
	move.l	backplane,$54(a6)
	move.w	#$02,$64(a6)
	move.w	#$00,$66(a6)
	move.w	#13*64+20,$58(a6)
	rts



;------------------------------------------------ scrollplane in plane
;						  kopieren (mit lines)
linecopy:
	lea	linetab2(pc),a2	
	lea	scrollplane,a3

	BLITWAIT
	move.w 	#44,$060(a6)			; planebreite in
	move.w 	#44,$066(a6)			; moduloregister
	move.w 	#$8000,$074(a6)			;
	move.w 	#$ffff,$044(a6)			;
	move.w 	#$0000,$046(a6)			;

lcloop:	movem.w	(a2)+,d0/d1/d2/d3	
	add.w	#176,d0
	add.w	#176,d2
	add.w	#200,d1
	add.w	#200,d3

	bsr	line
	lea	2(a3),a3
	cmp.l	#linetabend2,a2
	bne.s	lcloop
	rts

;------------------------------------------------  linie ziehen
;						   (mit blitter)
;in:	d0.w ... x1
;	d1.w ... y1
;	d2.w ... x2
;	d3.w ... y2
;trash:	d0-d5/a0

line:	move.l	backplane(pc),a0
	lea	oktabelle(pc),a1

	move.w	d1,d4
	mulu	#44,d4

	moveq 	#-$10,d5
	move.w	d0,d5
	lsr.w 	#3,d5				; x/8
	add.w 	d5,d4				;
	add.w  	d4,a0				;
	moveq	#0,d5				;
	sub.w 	d1,d3				; y2 - y1 = dy
	roxl.b 	#1,d5				;
	tst.w 	d3				;
	bge.s 	y2gy1				;
	neg.w 	d3				;
y2gy1:	sub.w	d0,d2				; x2 - x1 = dx
	roxl.b 	#1,d5
	tst.w 	d2
	bge.s 	x2gx1
	neg.w 	d2
x2gx1:	move.w 	d3,d1
	sub.w 	d2,d1
	bge.s 	dygdx
	exg 	d2,d3
dygdx:	roxl.b 	#1,d5
 	move.b 	(a1,d5.w),d5
	add.w 	d2,d2

	move.w 	d2,d4
	sub.w 	d3,d2
	bge.s 	signnl
	or.b 	#$40,d5
signnl:	and.w 	#$000f,d0
	move.w	d0,d1

	not.w	d1
	move.w	(a3)+,d6
;	move.w	#$ffff,d6
	rol.w	#1,d6
	ror.w 	#4,d0
	or.w 	#$0bca,d0

	swap	d0
	move.w	d5,d0
	
	move.w 	d2,d5
	sub.w 	d3,d2

;	lsl.w 	#6,d3
;	add.w 	#66,d3
	move.w	#64*16+2,d3

	move.w	d2,a5
	moveq	#4-1,d2

	BLITWAIT
;	bchg	d1,(a0)

	move.l 	d0,$040(a6)
	move.l 	a0,$048(a6)
	move.l 	a0,$054(a6)
	move.w 	a5,$064(a6)
	move.w	d4,$062(a6)
	move.w	d5,$052(a6)
	move.w	d6,$072(a6)
	move.w 	d3,$058(a6)

	moveq	#18,d0
lineloop:
	move.w	(a3)+,d6
	rol.w	#1,d6
	BLITWAIT
	move.w	d6,$72(a6)
	move.w 	d3,$058(a6)
	dbra	d0,lineloop

	lea	4(a3),a3
	rts






;------------------------------------------------ variablen
;
ipuf:		dc.l	0
intena:		dc.w	0
dmacon:		dc.w	0

scrollpos:	dc.w	0
winkel:		dc.w	0

frontplane:	dc.l	plane1
backplane:	dc.l	plane2

copperlist:
	dc.w	$0180,$0000
	dc.w	$0182,$0000
	dc.w	$0184,$0000
	dc.w	$0186,$0000
	dc.w	$0100,$2200
copperplanes:
	dc.w	$00e0,$0000
	dc.w	$00e2,$0000
	dc.w	$00e4,$0000
	dc.w	$00e6,$0000

	dc.w	$32d7,$fffe
	dc.w	$0100,$5600
	dc.w	$0190,$0000
	dc.w	$0192,$0000
	dc.w	$0194,$0000
	dc.w	$0196,$0000
	dc.w	$0100,$5600
copperplanes2:
	dc.w	$00e4,$0000
	dc.w	$00e6,$0000
	dc.w	$00ec,$0000
	dc.w	$00ee,$0000

logoplanes:
	dc.w	$00e0,$0000
	dc.w	$00e2,$0000
	dc.w	$00e8,$0000
	dc.w	$00ea,$0000
	dc.w	$00f0,$0000
	dc.w	$00f2,$0000

	dc.w	$0182,$0a97
	dc.w	$0184,$0a70
	dc.w	$0186,$0850
	dc.w	$0188,$0410
	dc.w	$018a,$0a83
	dc.w	$018c,$0630
	dc.w	$018e,$0000

colorlist:
	blk.w	33*4,0			; $450f - $650f

	dc.w	$5d0f,$fffe
	dc.w	$0100,$0000

	dc.w	$65d7,$fffe
copperplanes3:
	dc.w	$00e0,$0000
	dc.w	$00e2,$0000
	dc.w	$00e4,$0000
	dc.w	$00e6,$0000

	dc.w	$660f,$fffe
colorlist2:
	blk.w	120,0

	dc.w	$670f,$fffe
	dc.w	$0100,$2200
	dc.w	$0180,$0000
	dc.w	$0182,$0ddd
	dc.w	$0184,$0888
	dc.w	$0186,$0ddd
	
	dc.w	$ffe1,$fffe

	dc.w	$15d7,$fffe
	dc.w	$0100,$0000
	dc.w	$160f,$fffe
colorlist3:
	blk.w	120,0
	dc.w	$170f,$fffe
	dc.w	$0180,$0000
	dc.w	$0182,$0000
	dc.w	$0184,$0000
	dc.w	$0186,$0000
	
	dc.w	$ffff,$fffe

colorscrollpos:	dc.w	0

colors:
 dc.w	$00f,$01e,$02d,$03c,$04b,$05a,$069,$078,$087
 dc.w	$096,$0a5,$0b4,$0c3,$0d2,$0e1,$0f0,$1e0,$2d0,$3c0,$4b0,$5a0
 dc.w	$690,$780,$870,$960,$a50,$b40,$c30,$d20,$e10,$f00,$e01,$d02
 dc.w	$c03,$b04,$a05,$906,$807,$708,$609,$50a,$40b,$30c,$20d,$10e
 dc.w	$00f,$01e,$02d,$03c,$04b,$05a,$069,$078,$087
 dc.w	$096,$0a5,$0b4,$0c3,$0d2,$0e1,$0f0,$1e0,$2d0,$3c0,$4b0,$5a0
 dc.w	$690,$780,$870,$960,$a50,$b40,$c30,$d20,$e10,$f00,$e01,$d02
 dc.w	$c03,$b04,$a05,$906,$807,$708,$609,$50a,$40b,$30c,$20d,$10e
 dc.w	$00f,$01e,$02d,$03c,$04b,$05a,$069,$078,$087
 dc.w	$096,$0a5,$0b4,$0c3,$0d2,$0e1,$0f0,$1e0,$2d0,$3c0,$4b0,$5a0
 dc.w	$690,$780,$870,$960,$a50,$b40,$c30,$d20,$e10,$f00,$e01,$d02
 dc.w	$c03,$b04,$a05,$906,$807,$708,$609,$50a,$40b,$30c,$20d,$10e


chartable:
 dc.b	$35,$1a,$ff,$ff,$ff,$ff,$1b,$23,$1c,$1d,$1e,$1f,$32,$20,$31,$33
 dc.b	$2e,$25,$26,$27,$28,$29,$2a,$2b,$2c,$2d,$2f,$30,$ff,$21,$ff,$22
 dc.b	$ff,$00,$01,$02,$03,$04,$05,$06,$07,$08,$09,$0a,$0b,$0c,$0d,$0e
 dc.b	$0f,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$ff,$ff,$ff,$ff,$ff

char:		incbin "font2"
;blk.b	1728,$00			; zeichensatz
logo:	incbin "logo"
;	blk.b	5544,$00

sinustab:	incbin "sincos$8000"
;blk.b	1280,0

textpointer:	dc.l	text

; SCROLLSTEUERUNG:
; - $00		: Scroll-Ende
; - $01		: Drehrichtung �ndern
; - $02,wert	: Rotatespeed auf wert stellen
; - $03,wert	: Scrollspeed auf wert stellen
; - $04,wert	: Scroller wert ticks lang anhalten (50=1 Sekunde)
; - $05,wert	; winkel auf wert setzen

text:	dc.b 5,0,2,2,3,4
	dc.b "     SPREADPOINT IS PROUD TO PRESENT YOU ANOTHER RELEASE "
	dc.b "FOR FRIENDSHIP...                     "
	dc.b 2,0,5,20," XCOPY PROFESSIONAL  ",4,255," "

	dc.b 2,2,"     ...CRACKED BY MATHEW OF SPREADPOINT !           "
	dc.b "THIS LITTLE INTRO WAS CODED BY MATHEW. LOGO PAINTED BY "
	dc.b "MARVIN. THANX TO STEEL AND MASH OF SUBWAY FOR TUNE AND"
	dc.b " FONT !                          ",0
	even

oktabelle:
	dc.b	0*4+1
	dc.b	4*4+1
	dc.b 	2*4+1
	dc.b	5*4+1
	dc.b	1*4+1
	dc.b	6*4+1
	dc.b	3*4+1
	dc.b	7*4+1

linetab:
	dc.w	-174,-12,174,-12
	dc.w	-174,-10,174,-10
	dc.w	-174,-8,174,-8
	dc.w	-174,-6,174,-6
	dc.w	-174,-4,174,-4
	dc.w	-174,-2,174,-2
	dc.w	-174, 0,174, 0
	dc.w	-174, 2,174, 2
	dc.w	-174, 4,174, 4
	dc.w	-174, 6,174, 6
	dc.w	-174, 8,174, 8
	dc.w	-174,10,174,10
	dc.w	-174,12,174,12
linetabend:

linetab2:
	blk.w	4*13,0
linetabend2:

; -----------------------------------------------------
; ------- D.O.C SoundTracker V2.0 - playroutine -------
; -----------------------------------------------------
; ---- Improved and omptimized by Unknown of D.O.C ----
; --------- Based on the playroutine from TJC ---------
; -----------------------------------------------------
; ------ Adapted for SondTracker V2.3 Songs by  -------
; ----------- M.A.S.H. and Mathew of Subway. ----------
; -----------------------------------------------------      


mt_init:
	move.l	mt_point,a0
	move.b	$1d7(a0),d0
	and.b	#$f0,d0
	cmp.b	#$70,d0
	bne.s	Version2_3
	lea	LabelTab2_0,a0
	bra.s	No2_3
Version2_3:
	lea	LabelTab2_3,a0
No2_3:	
	lea	AdressTab,a1
ChgL:
	move.l	(a1)+,a2
	move.w	(a0)+,(a2)
	cmp.l	#-1,(a1)
	bne.s	ChgL
	move.l	mt_point,a0
Not2_3:	
	add.l	#$03b8,a0
	moveq	#$7f,d0				;$80 ?????
	moveq	#0,d1
mt_init1:
	move.l	d1,d2
	subq.w	#1,d0
mt_init2:
	move.b	(a0)+,d1
	cmp.b	d2,d1
	bgt.s	mt_init1
	dbf		d0,mt_init2
	addq.b	#1,d2

mt_init3:
	move.l	mt_point,a0
	lea		mt_sample1(pc),a1
	asl.l	#8,d2
	asl.l	#2,d2
mt_chg1:
	add.l	#$438,d2
	add.l	a0,d2
mt_chg2:
	moveq	#$1e,d0
mt_init4:
	move.l	d2,(a1)+
	moveq	#0,d1
	move.w	42(a0),d1
	asl.l	#1,d1
	add.l	d1,d2
	add.l	#$1e,a0
	dbf	d0,mt_init4

	lea		mt_sample1(PC),a0
	moveq	#0,d0
mt_clear:
	move.l	(a0,d0.w),a1
	clr.l	(a1)
	addq.w	#4,d0
mt_chg3: 
	cmp.w	#$7c,d0
	bne.s	mt_clear

	clr.w	$dff0a8
	clr.w	$dff0b8
	clr.w	$dff0c8
	clr.w	$dff0d8
	clr.l	mt_partnrplay
	clr.l	mt_partnote
	clr.l	mt_partpoint
	
	move.l	a0,-(a7)
	move.l	mt_point,a0
mt_chg4:
	add.l	#$3b6,a0
	move.b	(a0),mt_maxpart+1
	move.l	(a7)+,a0
	rts

mt_end:	
	clr.w	$dff0a8
	clr.w	$dff0b8
	clr.w	$dff0c8
	clr.w	$dff0d8
	move.w	#$f,$dff096
	rts

mt_music:
	movem.l	d0-d7/a0-a6,-(a7)
	addq.w	#1,mt_counter
mt_cool: 
	cmp.w	#6,mt_counter
	beq.s	mt_six
	bsr.s	mt_notsix
	bra.s	mt_end2
mt_six:	
	clr.w	mt_counter
	bsr 	mt_rout2
mt_end2: 
	movem.l	(a7)+,d0-d7/a0-a6
	rts

mt_notsix:
	lea	mt_aud1temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp1
	lea	$dff0a0,a5		
	bsr.s	mt_arprout
mt_arp1: 
	lea	mt_aud2temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp2
	lea	$dff0b0,a5
	bsr.s	mt_arprout
mt_arp2: 
	lea	mt_aud3temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp3
	lea	$dff0c0,a5
	bsr.s	mt_arprout
mt_arp3: 
	lea	mt_aud4temp(PC),a6
	tst.b	3(a6)
	beq.s	mt_arp4
	lea	$dff0d0,a5
	bra.s	mt_arprout
mt_arp4:
	rts

mt_arprout:
	move.b	2(a6),d0
	and.b	#$0f,d0
	tst.b	d0
	beq	mt_arpegrt
	cmp.b	#$01,d0
	beq.s	mt_portup
	cmp.b	#$02,d0
	beq.s	mt_portdwn
	cmp.b	#$0a,d0
	beq.s	mt_volslide
	rts

mt_portup:
	moveq	#0,d0
	move.b	3(a6),d0
	sub.w	d0,22(a6)
	cmp.w	#$71,22(a6)
	bpl.s	mt_ok1
	move.w	#$71,22(a6)
mt_ok1:	
	move.w	22(a6),6(a5)
	rts

mt_portdwn:
	moveq	#0,d0
	move.b	3(a6),d0
	add.w	d0,22(a6)
mt_chg5: 
	cmp.w	#$538,22(a6)
	bmi.s	mt_ok2
mt_chg6: 
	move.w	#$538,22(a6)
mt_ok2:	
	move.w	22(a6),6(a5)
	rts

mt_volslide:
	moveq	#0,d0
	move.b	3(a6),d0
	lsr.b	#4,d0
	tst.b	d0
	beq.s	mt_voldwn
	add.w	d0,18(a6)
	cmp.w	#64,18(a6)
	bmi.s	mt_ok3
	move.w	#64,18(a6)
mt_ok3:	
	move.w	18(a6),8(a5)
	rts
mt_voldwn:
	moveq	#0,d0
	move.b	3(a6),d0
	and.b	#$0f,d0
	sub.w	d0,18(a6)
	bpl.s	mt_ok4
	clr.w	18(a6)
mt_ok4:	
	move.w	18(a6),8(a5)
	rts

mt_arpegrt:
	move.w	mt_counter(PC),d0
	cmp.w	#1,d0
	beq.s	mt_loop2
	cmp.w	#2,d0
	beq.s	mt_loop3
	cmp.w	#3,d0
	beq.s	mt_loop4
	cmp.w	#4,d0
	beq.s	mt_loop2
	cmp.w	#5,d0
	beq.s	mt_loop3
	rts

mt_loop2:
	moveq	#0,d0
	move.b	3(a6),d0
	lsr.b	#4,d0
	bra.s	mt_cont
mt_loop3:
	moveq	#$00,d0
	move.b	3(a6),d0
	and.b	#$0f,d0
	bra.s	mt_cont
mt_loop4:
	move.w	16(a6),d2
	bra.s	mt_endpart
mt_cont:
	add.w	d0,d0
	moveq	#0,d1
	move.w	16(a6),d1
	lea	mt_arpeggio(PC),a0
mt_loop5:
	move.w	(a0,d0),d2
	cmp.w	(a0),d1
	beq.s	mt_endpart
	addq.l	#2,a0
	bra.s	mt_loop5
mt_endpart:
	move.w	d2,6(a5)
	rts

mt_rout2:
	move.l	mt_point,a0
	move.l	a0,a3
	add.l	#$0c,a3
	move.l	a0,a2
mt_chg7: 
	add.l	#$3b8,a2
mt_chg8: 
	add.l	#$43c,a0
	move.l	mt_partnrplay(PC),d0
	moveq	#0,d1
	move.b	(a2,d0),d1
	asl.l	#8,d1
	asl.l	#2,d1
	add.l	mt_partnote(PC),d1
	move.l	d1,mt_partpoint
	clr.w	mt_dmacon

	lea	volumes,a4
	lea	$dff0a0,a5
	lea	mt_aud1temp(PC),a6
	bsr	mt_playit
	lea	$dff0b0,a5
	lea	mt_aud2temp(PC),a6
	bsr	mt_playit
	lea	$dff0c0,a5
	lea	mt_aud3temp(PC),a6
	bsr	mt_playit
	lea	$dff0d0,a5
	lea	mt_aud4temp(PC),a6
	bsr	mt_playit
	move.w	#$01f4,d0
mt_rls:	
	dbf	d0,mt_rls

	move.w	#$8000,d0
	or.w	mt_dmacon,d0
	move.w	d0,$dff096

	lea	mt_aud4temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice3
	move.l	10(a6),$dff0d0
	move.w	#1,$dff0d4
mt_voice3:
	lea	mt_aud3temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice2
	move.l	10(a6),$dff0c0
	move.w	#1,$dff0c4
mt_voice2:
	lea	mt_aud2temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice1
	move.l	10(a6),$dff0b0
	move.w	#1,$dff0b4
mt_voice1:
	lea	mt_aud1temp(PC),a6
	cmp.w	#1,14(a6)
	bne.s	mt_voice0
	move.l	10(a6),$dff0a0
	move.w	#1,$dff0a4
mt_voice0:
	move.l	mt_partnote(PC),d0
	add.l	#$10,d0
	move.l	d0,mt_partnote
	cmp.l	#$400,d0
	bne.s	mt_stop
mt_higher:
	clr.l	mt_partnote
	addq.l	#1,mt_partnrplay
	moveq	#0,d0
	move.w	mt_maxpart(PC),d0
	move.l	mt_partnrplay(PC),d1
	cmp.l	d0,d1
	bne.s	mt_stop
	clr.l	mt_partnrplay

mt_stop: 
	tst.w	mt_status
	beq.s	mt_stop2
	clr.w	mt_status
	bra.s	mt_higher
mt_stop2:
	rts

mt_playit:
	move.l	(a0,d1.l),(a6)
	addq.l	#4,d1
	moveq	#0,d2
	move.b	2(a6),d2
	and.b	#$f0,d2
	lsr.b	#4,d2

	move.b	(a6),d0
	and.b	#$f0,d0
	or.b	d0,d2
	tst.b	d2
	beq.s	mt_nosamplechange

	moveq	#0,d3
	lea	mt_samples(PC),a1
	move.l	d2,d4
	asl.l	#2,d2
	mulu	#$1e,d4
	move.l	(a1,d2),4(a6)
	move.w	(a3,d4.l),8(a6)
	move.w	2(a3,d4.l),18(a6)
	move.w	4(a3,d4.l),d3
	tst.w	d3
	beq.s	mt_displace
	move.l	4(a6),d2
	add.l	d3,d2
	move.l	d2,4(a6)
	move.l	d2,10(a6)
	move.w	6(a3,d4.l),8(a6)
	move.w	6(a3,d4.l),14(a6)
	move.w	18(a6),8(a5)
	bra.s	mt_nosamplechange

mt_displace:
	move.l	4(a6),d2
	add.l	d3,d2
	move.l	d2,10(a6)
	move.w	6(a3,d4.l),14(a6)
	move.w	18(a6),8(a5)
mt_nosamplechange:
	tst.w	(a6)
	beq.s	mt_retrout
	move.w	(a6),16(a6)
	move.w	20(a6),$dff096
	move.l	4(a6),(a5)
	move.w	8(a6),4(a5)
	move.w	(a6),6(a5)
	move.w	20(a6),d0
	or.w	d0,mt_dmacon
	move.w	18(a6),(a4)+

mt_retrout:
	tst.w	(a6)
	beq.s	mt_nonewper
	move.w	(a6),22(a6)

mt_nonewper:
	move.b	2(a6),d0
	and.b	#$0f,d0
	cmp.b	#$0b,d0
	beq.s	mt_posjmp
	cmp.b	#$0c,d0
	beq.s	mt_setvol
	cmp.b	#$0d,d0
	beq.s	mt_break
	cmp.b	#$0e,d0
	beq.s	mt_setfil
	cmp.b	#$0f,d0
	beq.s	mt_setspeed
	rts

mt_posjmp:
	not.w	mt_status
	moveq	#0,d0
	move.b	3(a6),d0
	subq.b	#1,d0
	move.l	d0,mt_partnrplay
	rts

mt_setvol:
	move.b	3(a6),8(a5)
	rts

mt_break:
	not.w	mt_status
	rts

mt_setfil:
	moveq	#0,d0
	move.b	3(a6),d0
	and.b	#1,d0
	rol.b	#1,d0
	and.b	#$fd,$bfe001
	or.b	d0,$bfe001
	rts

mt_setspeed:
	move.b	3(a6),d0
	and.b	#$0f,d0
	beq.s	mt_back
	clr.w	mt_counter
	move.b	d0,mt_cool+3
mt_back: 
	rts

mt_aud1temp:
	blk.w	10,0
	dc.w	1
	blk.w	2,0
mt_aud2temp:
	blk.w	10,0
	dc.w	2
	blk.w	2,0
mt_aud3temp:
	blk.w	10,0
	dc.w	4
	blk.w	2,0
mt_aud4temp:
	blk.w	10,0
	dc.w	8
	blk.w	2,0

mt_partnote:	dc.l	0
mt_partnrplay:	dc.l	0
mt_counter:	dc.w	0
mt_partpoint:	dc.l	0
mt_samples:	dc.l	0
mt_sample1:	blk.l	31,0
mt_maxpart:	dc.w	0
mt_dmacon:	dc.w	0
mt_status:	dc.w	0

mt_arpeggio:
	dc.w $0358,$0328,$02fa,$02d0,$02a6,$0280,$025c
	dc.w $023a,$021a,$01fc,$01e0,$01c5,$01ac,$0194,$017d
	dc.w $0168,$0153,$0140,$012e,$011d,$010d,$00fe,$00f0
	dc.w $00e2,$00d6,$00ca,$00be,$00b4,$00aa,$00a0,$0097
	dc.w $008f,$0087,$007f,$0078,$0071,$0000,$0000,$0000

LabelTab2_0:
	dc.w	$1d8
	dc.w	$258
	dc.w	$700e
	dc.w	$003c
	dc.w	$1d6
	dc.w	$358
	dc.w	$358
	dc.w	$1d8
	dc.w	$258

LabelTab2_3:
	dc.w	$3b8
	dc.w	$438
	dc.w	$701e
	dc.w	$007c
	dc.w	$3b6
	dc.w	$538
	dc.w	$538
	dc.w	$3b8
	dc.w	$43c

AdressTab:
	dc.l	Not2_3+4
	dc.l	mt_chg1+4
	dc.l	mt_chg2
	dc.l	mt_chg3+2
	dc.l	mt_chg4+4
	dc.l	mt_chg5+2
	dc.l	mt_chg6+2
	dc.l	mt_chg7+4
	dc.l	mt_chg8+4
	dc.l	-1

mt_point:	
	dc.l	mt_mem		;Adresse der Sounddaten.

volumes:
	dc.l	0,0,0,0		;Lautst�rke der vier Kan�le.

mt_mem:	incbin "mod.popcorn"
;blk.b	57090,0
fini:
